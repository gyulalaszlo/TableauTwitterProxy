
import tabpy_client
import string
from twitterscraper import query_tweets
#from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import json
from datetime import datetime, timedelta

client = tabpy_client.Client('http://localhost:9004/')

tweet_cache_dict = {}

def twitterscraperproxy(searchbox, limit, lang, begindate, enddate, requested_field, treshold): 

    ### Creating Cache ID from query parameters ###
    limit = int(limit)
    begindate = datetime.strptime(begindate, '%Y-%m-%d').date()
    enddate = datetime.strptime(enddate, '%Y-%m-%d').date()
    treshold = int(treshold)
    workers = 10

    cache_id = searchbox + "_" + str(limit) + "_" + str(begindate) + "_" + str(enddate)
    print("cache_id" + cache_id)
    ### Check if cache ID already exists in cached queries ###
    if cache_id in tweet_cache_dict: 
        print("cache exists:" + cache_id + "checking timestamp")

        ### Check if cached query timestamp is within treshold ###
        if datetime.now() - tweet_cache_dict[cache_id]['cache_timestamp'] < timedelta(seconds=treshold):
            print("request within cache treshold, serving request from cache") 
            raw_tweets = tweet_cache_dict[cache_id]['cache_results']

        ### Downloading tweets ###
        else:
            print("treshold exceeded, requesting new tweets")           
            raw_tweets = query_tweets(searchbox, limit, lang, begindate, enddate, workers)
            cache_results = raw_tweets
            cache_timestamp = datetime.now()
            tweet_cache_dict[cache_id] = {'cache_timestamp':cache_timestamp, 'cache_results': cache_results} 
    ### Downloading tweets ###
    else:
        print("no cach else brach")
        raw_tweets = query_tweets(searchbox, limit, lang, begindate, enddate, workers)

        cache_results = raw_tweets
        cache_timestamp = datetime.now()
        tweet_cache_dict[cache_id] = {'cache_timestamp':cache_timestamp, 'cache_results': cache_results} 

    ### Sorting tweets to descendign based on ID ###
    tweet_list = [] 
    ordered_tweet_list = sorted(raw_tweets, key=lambda x: getattr(x, 'id'), reverse=True)
    result_count = len(ordered_tweet_list)

    tweet_id_list = []
    timestamp_list = []
    likes_list = []
    retweets_list = []
    user_list = []
    text_list = []
    fullname_list = []
    replies_list = []
    url_list = []
    
   

    for i in range(0, limit):
        if i+1 < result_count:
            tweet_id_list.append(ordered_tweet_list[i].id)
            timestamp_list.append(datetime.strftime(ordered_tweet_list[i].timestamp, '%Y-%m-%d %H:%M:%S'))
            fullname_list.append(ordered_tweet_list[i].fullname)
            replies_list.append(int(ordered_tweet_list[i].replies))
            url_list.append(ordered_tweet_list[i].url)
            likes_list.append(int(ordered_tweet_list[i].likes))
            retweets_list.append(int(ordered_tweet_list[i].retweets))
            user_list.append(ordered_tweet_list[i].user)
            text_list.append(ordered_tweet_list[i].text)
        else:
            tweet_id_list.append(None)
            timestamp_list.append(None)
            fullname_list.append(None)
            replies_list.append(None)
            url_list.append(None)
            likes_list.append(None)
            retweets_list.append(None)
            user_list.append(None)
            text_list.append(None)
    
  
    ### Return requested field of tweets ### 
    if requested_field == 'tweet_id':
        return tweet_id_list[0:int(limit)]

    if requested_field == 'timestamp':
        return timestamp_list[0:int(limit)]

    if requested_field == 'likes':
        return likes_list[0:int(limit)]

    if requested_field == 'retweets':
        return retweets_list[0:int(limit)]

    if requested_field == 'user':
        return user_list[0:int(limit)]

    if requested_field == 'text':
        return text_list[0:int(limit)]

    if requested_field == 'fullname':
        return fullname_list[0:int(limit)]  

    if requested_field == 'replies':
        return replies_list[0:int(limit)]

    if requested_field == 'url':
        return url_list[0:int(limit)]

client.deploy('twitterscraperproxy', twitterscraperproxy,'get tweet fields', override = True)
