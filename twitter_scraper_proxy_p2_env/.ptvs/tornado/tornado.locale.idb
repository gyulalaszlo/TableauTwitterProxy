�}q (X   membersq}q(X   _default_localeq}q(X   kindqX   dataqX   valueq}qX   typeq	X   __builtin__q
X   strq�qsuX   LOCALE_NAMESq}q(hhh}qh	h
X   dictq�qsuX   getq}q(hX   functionqh}q(X   docqX�  Returns the closest match for the given locale codes.

    We iterate over all given locale codes in order. If we have a tight
    or a loose match for the code (e.g., "en" for "en_US"), we return
    the locale. Otherwise we move to the next code in the list.

    By default we return ``en_US`` if no translations are found for any of
    the specified locales. You can change the default locale with
    `set_default_locale()`.qX   builtinq�X   locationqKAK�qX	   overloadsq]q}q(X   argsq}q(X
   arg_formatq X   *q!X   nameq"X   locale_codesq#h	]q$(h
X   tupleq%]q&ha�q'h
h%�q(h
h%]q)Na�q*eu�q+X   ret_typeq,]q-(X   tornado.localeq.X	   CSVLocaleq/�q0h.X   GettextLocaleq1�q2euauuX   PY3q3}q4(hhh}q5h	NsuX   codecsq6}q7(hX	   modulerefq8hX   codecsq9X    q:�q;uX   BytesIOq<}q=(hX   typerefq>h]q?X   _ioq@X   BytesIOqA�qBauX   load_gettext_translationsqC}qD(hhh}qE(hX  Loads translations from `gettext`'s locale tree

    Locale tree is similar to system's ``/usr/share/locale``, like::

        {directory}/{lang}/LC_MESSAGES/{domain}.mo

    Three steps are required to have your app translated:

    1. Generate POT translation file::

        xgettext --language=Python --keyword=_:1,2 -d mydomain file1.py file2.html etc

    2. Merge against existing POT file::

        msgmerge old.po mydomain.po > new.po

    3. Compile::

        msgfmt mydomain.po -o {directory}/pt_BR/LC_MESSAGES/mydomain.moqFh�hK�K�qGh]qH}qI(h}qJ(h"X	   directoryqKh	Nu}qL(h"X   domainqMh	Nu�qNh,NuauuX   gen_logqO}qP(hX   multipleqQh}qRh(}qS(hhh}qTh	h
X   NoneTypeqU�qVsu}qW(hhh}qXh	h
X   objectqY�qZsu}q[(hhh}q\h	X   loggingq]X   PlaceHolderq^�q_su}q`(hhh}qah	h]X
   RootLoggerqb�qcsu}qd(hhh}qeh	h]X   Loggerqf�qgsutqhsuX   numbersqi}qj(hh8hX   numbersqkh:�qluX   osqm}qn(hh8hX   osqoh:�qpuX   csvqq}qr(hh8hX   csvqsh:�qtuX   _translationsqu}qv(hhQh}qwh}qx(hhh}qyh	hsu}qz(hhh}q{h	hsu}q|(hhh}q}h	hsu�q~suX   _supported_localesq}q�(hhh}q�h	h
X	   frozensetq��q�suX   set_default_localeq�}q�(hhh}q�(hX2  Sets the default locale.

    The default locale is assumed to be the language used for all strings
    in the system. The translations loaded from disk are mappings from
    the default locale to the destination locale. Consequently, you don't
    need to create a translation file for the default locale.q�h�hKOK�q�h]q�}q�(h}q�(h"X   codeq�h	Nu�q�h,NuauuX   datetimeq�}q�(hh8hX   datetimeq�h:�q�uX   absolute_importq�}q�(hhh}q�h	X
   __future__q�X   _Featureq��q�suX   get_supported_localesq�}q�(hhh}q�(hX1   Returns a list of all the supported locale codes.q�h�hK�K�q�h]q�}q�(h)h,h�uauuX   load_translationsq�}q�(hhh}q�(hXr  Loads translations from CSV files in a directory.

    Translations are strings with optional Python-style named placeholders
    (e.g., ``My name is %(name)s``) and their associated translations.

    The directory should have translation files of the form ``LOCALE.csv``,
    e.g. ``es_GT.csv``. The CSV files should have two or three columns: string,
    translation, and an optional plural indicator. Plural indicators should
    be one of "plural" or "singular". A given string can have both singular
    and plural forms. For example ``%(name)s liked this`` may have a
    different verb conjugation depending on whether %(name)s is one
    name or a list of names. There should be two rows in the CSV file for
    that string, one with plural indicator "singular", and one "plural".
    For strings with no verbs that would change on translation, simply
    use "unknown" or the empty string (or don't include the column at all).

    The file is read using the `csv` module in the default "excel" dialect.
    In this format there should not be spaces after the commas.

    If no ``encoding`` parameter is given, the encoding will be
    detected automatically (among UTF-8 and UTF-16) if the file
    contains a byte-order marker (BOM), defaulting to UTF-8 if no BOM
    is present.

    Example translation ``es_LA.csv``::

        "I love you","Te amo"
        "%(name)s liked this","A %(name)s les gustó esto","plural"
        "%(name)s liked this","A %(name)s le gustó esto","singular"

    .. versionchanged:: 4.3
       Added ``encoding`` parameter. Added support for BOM-based encoding
       detection, UTF-16, and UTF-8-with-BOM.q�h�hK]K�q�h]q�}q�(h}q�(h"hKh	Nu}q�(h"X   encodingq�h	]q�(hhVeX   default_valueq�X   Noneq�u�q�h,NuauuX   _use_gettextq�}q�(hhh}q�h	h
X   boolq��q�suX   print_functionq�}q�(hhh}q�h	h�suX   divisionq�}q�(hhh}q�h	h�suX   escapeq�}q�(hh8hX   tornado.escapeq�h:�q�uX   CONTEXT_SEPARATORq�}q�(hhh}q�h	hsuX   req�}q�(hh8hX   req�h:�q�uh1}q�(hh	h}q�(X   mroq�]q�(h2h.X   Localeqǆq�hZeX   basesq�]q�h�ah}q�(X   __init__q�}q�(hhh}q�(hNh�hM�K	�q�h]q�}q�(h}q�(h"X   selfq�h	h2u}q�(h"h�h	hu}q�(h"X   translationsq�h	hVu�q�h,NuauuX   pgettextq�}q�(hhh}q�(hX6  Allows to set context for translation, accepts plural forms.

        Usage example::

            pgettext("law", "right")
            pgettext("good", "right")

        Plural message example::

            pgettext("organization", "club", "clubs", len(clubs))
            pgettext("stick", "club", "clubs", len(clubs))

        To generate POT file with context, add following options to step 1
        of `load_gettext_translations` sequence::

            xgettext [basic options] --keyword=pgettext:1c,2 --keyword=pgettext:1c,2,3

        .. versionadded:: 4.2q�h�hM�K	�q�h]q�}q�(h(}q�(h"h�h	h2u}q�(h"X   contextq�h	Nu}q�(h"X   messageq�h	Nu}q�(h"X   plural_messageq�h	hVh�h�u}q�(h"X   countq�h	hVh�h�utq�h,NuauuX	   translateq�}q�(hhh}q�(hNh�hM�K	�q�h]q�}q�(h(}q�(h"h�h	h2u}q�(h"h�h	hu}q�(h"h�h	hVh�h�u}q�(h"h�h	hVh�h�utq�h,NuauuX   ngettextq�}q�(hhh}q�h	NsuX   gettextq�}q�(hhh}q�h	NsuX   codeq�}q�(hhQh}q�h}q�(hhh}q�h	hsu}q�(hhh}q h	hsu�r  suX   namer  }r  (hhQh}r  h}r  (hhh}r  h	hZsu}r  (hhh}r  h	h
X   unicoder	  �r
  su�r  suX   rtlr  }r  (hhh}r  h	h�suh�}r  (hhh}r  h	hVsuX   _monthsr  }r  (hhQh}r  h}r  (hhh}r  h	h
X   listr  ]r  (NNNNNNNNNNNNe�r  su}r  (hhh}r  h	j  su}r  (hhh}r  h	j  su�r  suX	   _weekdaysr  }r  (hhQh}r   h}r!  (hhh}r"  h	h
j  ]r#  (NNNNNNNe�r$  su}r%  (hhh}r&  h	j$  su}r'  (hhh}r(  h	j$  su�r)  suuhX1   Locale implementation using the `gettext` module.r*  h�hM�K�r+  uuh/}r,  (hh	h}r-  (h�]r.  (h0h�hZeh�]r/  h�ah}r0  (h�}r1  (hhh}r2  (hNh�hM�K	�r3  h]r4  }r5  (h(}r6  (h"h�h	h0u}r7  (h"h�h	]r8  (hVheu}r9  (h"h�h	hVh�h�u}r:  (h"h�h	hVh�h�utr;  h,Nuauuh�}r<  (hhh}r=  (hNh�hM�K	�r>  h]r?  }r@  (h(}rA  (h"h�h	h0u}rB  (h"h�h	Nu}rC  (h"h�h	Nu}rD  (h"h�h	hVh�h�u}rE  (h"h�h	hVh�h�utrF  h,Nuauuh�}rG  (hhQh}rH  h}rI  (hhh}rJ  h	hVsu}rK  (hhh}rL  h	hsu�rM  suh�}rN  (hhQh}rO  h}rP  (hhh}rQ  h	hsu}rR  (hhh}rS  h	hsu�rT  suj  }rU  (hhQh}rV  h}rW  (hhh}rX  h	hZsu}rY  (hhh}rZ  h	j
  su�r[  suj  }r\  (hhh}r]  h	h�suj  }r^  (hhh}r_  h	h
j  �r`  suj  }ra  (hhh}rb  h	j`  suuhX=   Locale implementation using tornado's CSV translation format.rc  h�hM�K�rd  uuh�}re  (hh	h}rf  (h�]rg  (h�hZeh�]rh  hZah}ri  (h�}rj  (hhh}rk  (hX  Returns the translation for the given message for this locale.

        If ``plural_message`` is given, you must also provide
        ``count``. We return ``plural_message`` when ``count != 1``,
        and we return the singular form for the given message when
        ``count == 1``.rl  h�hM'K	�rm  h]rn  }ro  (h(}rp  (h"h�h	h�u}rq  (h"h�h	hu}rr  (h"h�h	]rs  (hVheh�h�u}rt  (h"h�h	hVh�h�utru  h,Nuauuh�}rv  (hhh}rw  (hNh�hMK	�rx  h]ry  }rz  (h}r{  (h"h�h	]r|  (h�h0h2eu}r}  (h"h�h	hu}r~  (h"h�h	]r  (hVheu�r�  h,NuauuX   listr�  }r�  (hhh}r�  (hX�   Returns a comma-separated list for the given list of parts.

        The format is, e.g., "A, B and C", "A and B" or just "A" for lists
        of size 1.r�  h�hM�K	�r�  h]r�  }r�  (h}r�  (h"h�h	h�u}r�  (h"X   partsr�  h	Nu�r�  h,huauuX   format_dater�  }r�  (hhh}r�  (hX�  Formats the given date (which should be GMT).

        By default, we return a relative time (e.g., "2 minutes ago"). You
        can return an absolute date string with ``relative=False``.

        You can force a full format date ("July 10, 1980") with
        ``full_format=True``.

        This method is primarily intended for dates in the past.
        For dates in the future, we fall back to full format.r�  h�hM4K	�r�  h]r�  }r�  (h(}r�  (h"h�h	h�u}r�  (h"X   dater�  h	]r�  (hkX   Realr�  �r�  h�h��r�  hZeu}r�  (h"X
   gmt_offsetr�  h	h
X   intr�  �r�  h�X   0r�  u}r�  (h"X   relativer�  h	h�h�X   Truer�  u}r�  (h"X   shorterr�  h	h�h�X   Falser�  u}r�  (h"X   full_formatr�  h	h�h�j�  utr�  h,Nuauuh}r�  (hhh}r�  (hXe   Returns the Locale for the given locale code.

        If it is not supported, we raise an exception.r�  h�hM K	�r�  h]r�  }r�  (h}r�  (h"X   clsr�  h	h�u}r�  (h"h�h	hu�r�  h,]r�  (h0h2euaX   classmethodr�  �uuX   friendly_numberr�  }r�  (hhh}r�  (hX7   Returns a comma-separated number for the given integer.r�  h�hM�K	�r�  h]r�  }r�  (h}r�  (h"h�h	h�u}r�  (h"X   valuer�  h	hu�r�  h,huauuX   get_closestr�  }r�  (hhh}r�  (hX4   Returns the closest match for the given locale code.r�  h�hK�K	�r�  h]r�  }r�  (h}r�  (h"j�  h	h�u}r�  (h h!h"h#h	]r�  (h*h'eu�r�  h,]r�  (h0h2euaj�  �uuX
   format_dayr�  }r�  (hhh}r�  (hX�   Formats the given date as a day of week.

        Example: "Monday, January 22". You can remove the day of week with
        ``dow=False``.r�  h�hM�K	�r�  h]r�  }r�  (h(}r�  (h"h�h	h�u}r�  (h"j�  h	Nu}r�  (h"j�  h	j�  h�X   0r�  u}r�  (h"X   dowr�  h	h�h�j�  utr�  h,Nuauuh�}r�  (hhh}r�  (hNh�hM1K	�r�  h]r�  }r�  (h(}r�  (h"h�h	h�u}r�  (h"h�h	Nu}r�  (h"h�h	Nu}r�  (h"h�h	hVh�h�u}r�  (h"h�h	hVh�h�utr�  h,NuauuX   _cacher�  }r�  (hhQh}r�  h(}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsu}r�  (hhh}r�  h	hsutr�  suh�}r�  (hhh}r�  h	Nsuj  }r�  (hhQh}r�  h}r   (hhh}r  h	j
  su}r  (hhh}r  h	hZsu�r  suj  }r  (hhh}r  h	h�suh�}r  (hhh}r  h	Nsuj  }r	  (hhh}r
  h	j  suj  }r  (hhh}r  h	j$  suuhX�   Object representing a locale.

    After calling one of `load_translations` or `load_gettext_translations`,
    call `get` or `get_closest` to get a Locale object.r  h�hK�K�r  uuuhX�  Translation methods for generating localized strings.

To load a locale and generate a translated string::

    user_locale = tornado.locale.get("es_LA")
    print(user_locale.translate("Sign out"))

`tornado.locale.get()` returns the closest matching locale, not necessarily the
specific locale you requested. You can support pluralization with
additional arguments to `~Locale.translate()`, e.g.::

    people = [...]
    message = user_locale.translate(
        "%(list)s is online", "%(list)s are online", len(people))
    print(message % {"list": user_locale.list(people)})

The first string is chosen if ``len(people) == 1``, otherwise the second
string is chosen.

Applications should call one of `load_translations` (which uses a simple
CSV format) or `load_gettext_translations` (which uses the ``.mo`` format
supported by `gettext` and related tools).  If neither method is called,
the `Locale.translate` method will simply return the original string.r  X   childrenr  ]r  X   filenamer  X|   c:\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\lib\site-packages\tornado\locale.pyr  u.