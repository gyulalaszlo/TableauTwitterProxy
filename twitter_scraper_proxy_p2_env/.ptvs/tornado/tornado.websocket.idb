�}q (X   membersq}q(X   print_functionq}q(X   kindqX   dataqX   valueq}qX   typeq	X
   __future__q
X   _Featureq�qsuX   utf8q}q(hX   funcrefqh}qX	   func_nameqX   tornado.escape.utf8qsuX   divisionq}q(hhh}qh	hsuX   collectionsq}q(hX	   modulerefqhX   collectionsqX    q�quX   TracebackFutureq}q(hX   typerefqh]qX   tornado.concurrentq X   Futureq!�q"auX   WebSocketHandlerq#}q$(hh	h}q%(X   mroq&]q'(X   tornado.websocketq(h#�q)X   tornado.webq*X   RequestHandlerq+�q,X   __builtin__q-X   objectq.�q/eX   basesq0]q1h,ah}q2(X   write_messageq3}q4(hX   functionq5h}q6(X   docq7Xe  Sends the given message to the client of this Web Socket.

        The message may be either a string or a dict (which will be
        encoded as json).  If the ``binary`` argument is false, the
        message will be sent as utf8; in binary mode any byte string
        is allowed.

        If the connection is already closed, raises `WebSocketClosedError`.

        .. versionchanged:: 3.2
           `WebSocketClosedError` was added (previously a closed connection
           would raise an `AttributeError`)

        .. versionchanged:: 4.3
           Returns a `.Future` which can be used for flow control.q8X   builtinq9�X   locationq:K�K	�q;X	   overloadsq<]q=}q>(X   argsq?}q@(X   nameqAX   selfqBh	h)u}qC(hAX   messageqDh	]qE(h-X   strqF�qGh-X   dictqH�qIeu}qJ(hAX   binaryqKh	h-X   boolqL�qMX   default_valueqNX   FalseqOu�qPX   ret_typeqQ]qR(h"h-X   NoneTypeqS�qTeuauuX   openqU}qV(hh5h}qW(h7X�   Invoked when a new WebSocket is opened.

        The arguments to `open` are extracted from the `tornado.web.URLSpec`
        regular expression, just like the arguments to
        `tornado.web.RequestHandler.get`.qXh9�h:M#K	�qYh<]qZ}q[(h?}q\(hAhBh	h)u}q](X
   arg_formatq^X   *q_hAX   argsq`h	]qa(h-X   tupleqb]qcNa�qdh-hb�qeeu}qf(h^X   **qghAX   kwargsqhh	hIu�qihQNuauuX   get_websocket_protocolqj}qk(hh5h}ql(h7Nh9�h:M�K	�qmh<]qn}qo(h?}qp(hAhBh	h)u�qqhQh(X   WebSocketProtocol13qr�qsuauuX   max_message_sizeqt}qu(hX   propertyqvh}qw(h7X�   Maximum allowed message size.

        If the remote peer sends a message larger than this, the connection
        will be closed.

        Default is 10MiB.qxh	Nh:K�K	�qyuuX   on_pingqz}q{(hh5h}q|(h7X*   Invoked when the a ping frame is received.q}h9�h:MAK	�q~h<]q}q�(h?}q�(hAhBh	h)u}q�(hAX   dataq�h	Nu�q�hQNuauuX   _break_cyclesq�}q�(hh5h}q�(h7Nh9�h:M�K	�q�h<]q�}q�(h?}q�(hAhBh	h)u�q�hQNuauuX   streamq�}q�(hhh}q�h	hTsuX   ping_intervalq�}q�(hhvh}q�(h7Xg   The interval for websocket keep-alive pings.

        Set websocket_ping_interval = 0 to disable pings.q�h	Nh:K�K	�q�uuX   __init__q�}q�(hh5h}q�(h7Nh9�h:K�K	�q�h<]q�}q�(h?(}q�(hAhBh	h)u}q�(hAX   applicationq�h	Nu}q�(hAX   requestq�h	Nu}q�(h^hghAhhh	hIutq�hQNuauuX   pingq�}q�(hh5h}q�(h7X"   Send ping frame to the remote end.q�h9�h:M7K	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAh�h	Nu�q�hQNuauuX   set_nodelayq�}q�(hh5h}q�(h7X(  Set the no-delay flag for this stream.

        By default, small messages may be delayed and/or combined to minimize
        the number of packets sent.  This can sometimes cause 200-500ms delays
        due to the interaction between Nagle's algorithm and TCP delayed
        ACKs.  To reduce this delay (at the expense of possibly increasing
        bandwidth usage), call ``self.set_nodelay(True)`` once the websocket
        connection is established.

        See `.BaseIOStream.set_nodelay` for additional details.

        .. versionadded:: 3.1q�h9�h:M�K	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAX   valueq�h	Nu�q�hQNuauuX   select_subprotocolq�}q�(hh5h}q�(h7X�  Invoked when a new WebSocket requests specific subprotocols.

        ``subprotocols`` is a list of strings identifying the
        subprotocols proposed by the client.  This method may be
        overridden to return one of those strings to select it, or
        ``None`` to not select a subprotocol.  Failure to select a
        subprotocol does not automatically abort the connection,
        although clients may close the connection if none of their
        proposed subprotocols was selected.q�h9�h:K�K	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAX   subprotocolsq�h	]q�(h-X   listq�]q�Na�q�hTeu�q�hQhTuauuX   get_compression_optionsq�}q�(hh5h}q�(h7X�  Override to return compression options for the connection.

        If this method returns None (the default), compression will
        be disabled.  If it returns a dict (even an empty one), it
        will be enabled.  The contents of the dict may be used to
        control the following compression options:

        ``compression_level`` specifies the compression level.

        ``mem_level`` specifies the amount of memory used for the internal compression state.

         These parameters are documented in details here:
         https://docs.python.org/3.6/library/zlib.html#zlib.compressobj

        .. versionadded:: 4.1

        .. versionchanged:: 4.5

           Added ``compression_level`` and ``mem_level``.q�h9�h:MK	�q�h<]q�}q�(h?}q�(hAhBh	h)u�q�hQhTuauuX   ping_timeoutq�}q�(hhvh}q�(h7X�   If no ping is received in this many seconds,
        close the websocket connection (VPNs, etc. can fail to cleanly close ws connections).
        Default is max of 3 pings or 30 seconds.q�h	Nh:K�K	�q�uuX   check_originq�}q�(hh5h}q�(h7X�  Override to enable support for allowing alternate origins.

        The ``origin`` argument is the value of the ``Origin`` HTTP
        header, the url responsible for initiating this request.  This
        method is not called for clients that do not send this header;
        such requests are always allowed (because all browsers that
        implement WebSockets support this header, and non-browser
        clients do not have the same cross-site security concerns).

        Should return True to accept the request or False to reject it.
        By default, rejects all requests with an origin on a host other
        than this one.

        This is a security protection against cross site scripting attacks on
        browsers, since WebSockets are allowed to bypass the usual same-origin
        policies and don't use CORS headers.

        .. warning::

           This is an important security measure; don't disable it
           without understanding the security implications. In
           particular, if your authentication is cookie-based, you
           must either restrict the origins allowed by
           ``check_origin()`` or implement your own XSRF-like
           protection for websocket connections. See `these
           <https://www.christian-schneider.net/CrossSiteWebSocketHijacking.html>`_
           `articles
           <https://devcenter.heroku.com/articles/websocket-security>`_
           for more.

        To accept all cross-origin traffic (which was the default prior to
        Tornado 4.0), simply override this method to always return true::

            def check_origin(self, origin):
                return True

        To allow connections from any subdomain of your site, you might
        do something like::

            def check_origin(self, origin):
                parsed_origin = urllib.parse.urlparse(origin)
                return parsed_origin.netloc.endswith(".mydomain.com")

        .. versionadded:: 4.0q�h9�h:MfK	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAX   originq�h	Nu�q�hQNuauuX   getq�}q�(hhh}q�hX   tornado.web.wrapperq�suX   on_pongq�}q�(hh5h}q�(h7X6   Invoked when the response to a ping frame is received.q�h9�h:M=K	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAh�h	Nu�q�hQNuauuX
   on_messageq�}q�(hh5h}q�(h7X�   Handle incoming messages on the WebSocket

        This method must be overridden.

        .. versionchanged:: 4.5

           ``on_message`` can be a coroutine.q�h9�h:M,K	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAhDh	Nu�q�hQNuauuX   closeq�}q�(hh5h}q�(h7X:  Closes this Web Socket.

        Once the close handshake is successful the socket will be closed.

        ``code`` may be a numeric status code, taken from the values
        defined in `RFC 6455 section 7.4.1
        <https://tools.ietf.org/html/rfc6455#section-7.4.1>`_.
        ``reason`` may be a textual message about why the connection is
        closing.  These values are made available to the client, but are
        not otherwise interpreted by the websocket protocol.

        .. versionchanged:: 4.0

           Added the ``code`` and ``reason`` arguments.q�h9�h:MRK	�q�h<]q�}q�(h?}q�(hAhBh	h)u}q�(hAX   codeq h	hThNX   Noner  u}r  (hAX   reasonr  h	hThNj  u�r  hQNuauuX
   send_errorr  }r  (hh5h}r  (h7Nh9�h:M�K	�r  h<]r	  }r
  (h?}r  (hAhBh	h)u}r  (h^h_hAh`h	heu}r  (h^hghAhhh	hIu�r  hQNuauuX   _attach_streamr  }r  (hh5h}r  (h7Nh9�h:M�K	�r  h<]r  }r  (h?}r  (hAhBh	h)u�r  hQNuauuX   on_connection_closer  }r  (hh5h}r  (h7Nh9�h:M�K	�r  h<]r  }r  (h?}r  (hAhBh	h)u�r  hQNuauuX   on_closer  }r   (hh5h}r!  (h7XW  Invoked when the WebSocket is closed.

        If the connection was closed cleanly and a status code or reason
        phrase was supplied, these values will be available as the attributes
        ``self.close_code`` and ``self.close_reason``.

        .. versionchanged:: 4.0

           Added ``close_code`` and ``close_reason`` attributes.r"  h9�h:MEK	�r#  h<]r$  }r%  (h?}r&  (hAhBh	h)u�r'  hQNuauuX   ws_connectionr(  }r)  (hX   multipler*  h}r+  h}r,  (hhh}r-  h	hTsu}r.  (hhh}r/  h	hssu�r0  suX
   close_coder1  }r2  (hhh}r3  h	hTsuX   close_reasonr4  }r5  (hj*  h}r6  h(}r7  (hhh}r8  h	hTsu}r9  (hhh}r:  h	h/su}r;  (hhh}r<  h	hGsu}r=  (hhh}r>  h	h-X   unicoder?  �r@  sutrA  suX   _on_close_calledrB  }rC  (hhh}rD  h	hMsuX	   open_argsrE  }rF  (hj*  h}rG  h}rH  (hhh}rI  h	hesu}rJ  (hhh}rK  h	hdsu�rL  suX   open_kwargsrM  }rN  (hj*  h}rO  h}rP  (hhh}rQ  h	hIsu}rR  (hhh}rS  h	hIsu�rT  suX   _status_coderU  }rV  (hhh}rW  h	h-X   intrX  �rY  suX   _reasonrZ  }r[  (hj*  h}r\  h}r]  (hhh}r^  h	h/su}r_  (hhh}r`  h	hGsu}ra  (hhh}rb  h	j@  su�rc  suX   _write_bufferrd  }re  (hhh}rf  h	h-hrg  suX	   _finishedrh  }ri  (hhh}rj  h	hMsuX   _headers_writtenrk  }rl  (hhh}rm  h	hMsuX   uirn  }ro  (hhh}rp  h	hTsuuh7X�  Subclass this class to create a basic WebSocket handler.

    Override `on_message` to handle incoming messages, and use
    `write_message` to send messages to the client. You can also
    override `open` and `on_close` to handle opened and closed
    connections.

    Custom upgrade response headers can be sent by overriding
    `~tornado.web.RequestHandler.set_default_headers` or
    `~tornado.web.RequestHandler.prepare`.

    See http://dev.w3.org/html5/websockets/ for details on the
    JavaScript interface.  The protocol is specified at
    http://tools.ietf.org/html/rfc6455.

    Here is an example WebSocket handler that echos back all received messages
    back to the client:

    .. testcode::

      class EchoWebSocket(tornado.websocket.WebSocketHandler):
          def open(self):
              print("WebSocket opened")

          def on_message(self, message):
              self.write_message(u"You said: " + message)

          def on_close(self):
              print("WebSocket closed")

    .. testoutput::
       :hide:

    WebSockets are not standard HTTP connections. The "handshake" is
    HTTP, but after the handshake, the protocol is
    message-based. Consequently, most of the Tornado HTTP facilities
    are not available in handlers of this type. The only communication
    methods available to you are `write_message()`, `ping()`, and
    `close()`. Likewise, your request handler class should implement
    `open()` method rather than ``get()`` or ``post()``.

    If you map the handler above to ``/websocket`` in your application, you can
    invoke it in JavaScript with::

      var ws = new WebSocket("ws://localhost:8888/websocket");
      ws.onopen = function() {
         ws.send("Hello, world");
      };
      ws.onmessage = function (evt) {
         alert(evt.data);
      };

    This script pops up an alert box that says "You said: Hello, world".

    Web browsers allow any site to open a websocket connection to any other,
    instead of using the same-origin policy that governs other network
    access from javascript.  This can be surprising and is a potential
    security hole, so since Tornado 4.0 `WebSocketHandler` requires
    applications that wish to receive cross-origin websockets to opt in
    by overriding the `~WebSocketHandler.check_origin` method (see that
    method's docs for details).  Failure to do so is the most likely
    cause of 403 errors when making a websocket connection.

    When using a secure websocket connection (``wss://``) with a self-signed
    certificate, the connection from a browser may fail because it wants
    to show the "accept this certificate" dialog but has nowhere to show it.
    You must first visit a regular HTML page using the same certificate
    to accept it before the websocket connection will succeed.

    If the application setting ``websocket_ping_interval`` has a non-zero
    value, a ping will be sent periodically, and the connection will be
    closed if a response is not received before the ``websocket_ping_timeout``.

    Messages larger than the ``websocket_max_message_size`` application setting
    (default 10MiB) will not be accepted.

    .. versionchanged:: 4.5
       Added ``websocket_ping_interval``, ``websocket_ping_timeout``, and
       ``websocket_max_message_size``.rq  h9�h:K<K�rr  uuX   WebSocketErrorrs  }rt  (hh	h}ru  (h&]rv  (h(js  �rw  X
   exceptionsrx  X	   Exceptionry  �rz  jx  X   BaseExceptionr{  �r|  h/h-jy  �r}  eh0]r~  j}  ah}r  h7Nh9�h:K0K�r�  uuX   _PerMessageDeflateCompressorr�  }r�  (hh	h}r�  (h&]r�  (h(j�  �r�  h/eh0]r�  h/ah}r�  (h�}r�  (hh5h}r�  (h7Nh9�h:MK	�r�  h<]r�  }r�  (h?(}r�  (hAhBh	j�  u}r�  (hAX
   persistentr�  h	hMu}r�  (hAX	   max_wbitsr�  h	jY  u}r�  (hAX   compression_optionsr�  h	hThNj  utr�  hQNuauuX   _create_compressorr�  }r�  (hh5h}r�  (h7Nh9�h:MK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j�  u�r�  hQNuauuX   compressr�  }r�  (hh5h}r�  (h7Nh9�h:M K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j�  u}r�  (hAh�h	hGu�r�  hQhGuauuX
   _max_wbitsr�  }r�  (hj*  h}r�  h}r�  (hhh}r�  h	jY  su}r�  (hhh}r�  h	jY  su�r�  suX   _compression_levelr�  }r�  (hhh}r�  h	jY  suX
   _mem_levelr�  }r�  (hhh}r�  h	jY  suX   _compressorr�  }r�  (hhh}r�  h	hTsuuh7Nh9�h:MK�r�  uuX
   to_unicoder�  }r�  (hhh}r�  hX   tornado.escape.to_unicoder�  suX   structr�  }r�  (hhhX   structr�  h�r�  uX
   httpclientr�  }r�  (hhhX   tornado.httpclientr�  h�r�  uX   websocket_connectr�  }r�  (hh5h}r�  (h7X�  Client-side websocket support.

    Takes a url and returns a Future whose result is a
    `WebSocketClientConnection`.

    ``compression_options`` is interpreted in the same way as the
    return value of `.WebSocketHandler.get_compression_options`.

    The connection supports two styles of operation. In the coroutine
    style, the application typically calls
    `~.WebSocketClientConnection.read_message` in a loop::

        conn = yield websocket_connect(url)
        while True:
            msg = yield conn.read_message()
            if msg is None: break
            # Do something with msg

    In the callback style, pass an ``on_message_callback`` to
    ``websocket_connect``. In both styles, a message of ``None``
    indicates that the connection has been closed.

    .. versionchanged:: 3.2
       Also accepts ``HTTPRequest`` objects in place of urls.

    .. versionchanged:: 4.1
       Added ``compression_options`` and ``on_message_callback``.
       The ``io_loop`` argument is deprecated.

    .. versionchanged:: 4.5
       Added the ``ping_interval``, ``ping_timeout``, and ``max_message_size``
       arguments, which have the same meaning as in `WebSocketHandler`.r�  h9�h:M�K�r�  h<]r�  }r�  (h?(}r�  (hAX   urlr�  h	j�  X   HTTPRequestr�  �r�  u}r�  (hAX   io_loopr�  h	]r�  (X   tornado.platform.selectr�  X   SelectIOLoopr�  �r�  X   tornado.utilr�  X   Configurabler�  �r�  hTX   tornado.ioloopr�  X   IOLoopr�  �r�  X   tornado.platform.epollr�  X   EPollIOLoopr�  �r�  X   tornado.platform.kqueuer�  X   KQueueIOLoopr�  �r�  X   tornado.netutilr�  X   Resolverr�  �r�  j�  X   OverrideResolverr�  �r�  X   tornado.httpserverr�  X
   HTTPServerr�  �r�  ehNj  u}r�  (hAX   callbackr�  h	hThNj  u}r�  (hAX   connect_timeoutr�  h	hThNj  u}r�  (hAX   on_message_callbackr�  h	hThNj  u}r�  (hAj�  h	hThNj  u}r�  (hAh�h	hThNj  u}r�  (hAh�h	hThNj  u}r�  (hAhth	hThNj  utr�  hQh"uauuX   _PerMessageDeflateDecompressorr�  }r�  (hh	h}r�  (h&]r�  (h(j�  �r�  h/eh0]r�  h/ah}r�  (h�}r�  (hh5h}r�  (h7Nh9�h:M)K	�r�  h<]r�  }r�  (h?(}r   (hAhBh	j�  u}r  (hAj�  h	hMu}r  (hAj�  h	jY  u}r  (hAj�  h	hThNj  utr  hQNuauuX
   decompressr  }r  (hh5h}r  (h7Nh9�h:M8K	�r  h<]r	  }r
  (h?}r  (hAhBh	j�  u}r  (hAh�h	]r  (hGhTeu�r  hQNuauuX   _create_decompressorr  }r  (hh5h}r  (h7Nh9�h:M5K	�r  h<]r  }r  (h?}r  (hAhBh	j�  u�r  hQNuauuj�  }r  (hj*  h}r  h}r  (hhh}r  h	jY  su}r  (hhh}r  h	jY  su�r  suX   _decompressorr  }r  (hhh}r   h	hTsuuh7Nh9�h:M(K�r!  uuX   zlibr"  }r#  (hhhX   zlibr$  h�r%  uX   gen_logr&  }r'  (hj*  h}r(  h(}r)  (hhh}r*  h	hTsu}r+  (hhh}r,  h	h/su}r-  (hhh}r.  h	X   loggingr/  X   PlaceHolderr0  �r1  su}r2  (hhh}r3  h	j/  X   Loggerr4  �r5  su}r6  (hhh}r7  h	j/  X
   RootLoggerr8  �r9  sutr:  suX   WebSocketClientConnectionr;  }r<  (hh	h}r=  (h&]r>  (h(j;  �r?  X   tornado.simple_httpclientr@  X   _HTTPConnectionrA  �rB  X   tornado.httputilrC  X   HTTPMessageDelegaterD  �rE  h/eh0]rF  jB  ah}rG  (X   read_messagerH  }rI  (hh5h}rJ  (h7Xo  Reads a message from the WebSocket server.

        If on_message_callback was specified at WebSocket
        initialization, this function will never return messages

        Returns a future whose result is the message, or None
        if the connection is closed.  If a callback argument
        is given it will be called with the future when it is
        ready.rK  h9�h:MzK	�rL  h<]rM  }rN  (h?}rO  (hAhBh	j?  u}rP  (hAj�  h	hThNj  u�rQ  hQh"uauuhj}rR  (hh5h}rS  (h7Nh9�h:M�K	�rT  h<]rU  }rV  (h?}rW  (hAhBh	j?  u�rX  hQhsuauuX   headers_receivedrY  }rZ  (hh5h}r[  (h7Nh9�h:M]K	�r\  h<]r]  }r^  (h?}r_  (hAhBh	j?  u}r`  (hAX
   start_linera  h	Nu}rb  (hAX   headersrc  h	Nu�rd  hQNuauuh�}re  (hh5h}rf  (h7Nh9�h:M�K	�rg  h<]rh  }ri  (h?}rj  (hAhBh	j?  u}rk  (hAhDh	hTu�rl  hQNuauuh�}rm  (hh5h}rn  (h7Nh9�h:MK	�ro  h<]rp  }rq  (h?(}rr  (hAhBh	j?  u}rs  (hAj�  h	]rt  (j�  j�  hTj�  j�  j�  j�  j�  j�  eu}ru  (hAh�h	]rv  (j�  X   _RequestProxyrw  �rx  j�  eu}ry  (hAj�  h	hThNj  u}rz  (hAj�  h	hThNj  u}r{  (hAh�h	hThNj  u}r|  (hAh�h	hThNj  u}r}  (hAhth	hThNj  utr~  hQNuauuj  }r  (hh5h}r�  (h7Nh9�h:MNK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u�r�  hQNuauuX   _on_http_responser�  }r�  (hh5h}r�  (h7Nh9�h:MUK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u}r�  (hAX   responser�  h	Nu�r�  hQNuauuh3}r�  (hh5h}r�  (h7X(   Sends a message to the WebSocket server.r�  h9�h:MvK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u}r�  (hAhDh	Nu}r�  (hAhKh	hMhNhOu�r�  hQ]r�  (h"hTeuauuh�}r�  (hh5h}r�  (h7Nh9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u}r�  (hAh�h	Nu�r�  hQNuauuhz}r�  (hh5h}r�  (h7Nh9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u}r�  (hAh�h	Nu�r�  hQNuauuh�}r�  (hh5h}r�  (h7X�   Closes the websocket connection.

        ``code`` and ``reason`` are documented under
        `WebSocketHandler.close`.

        .. versionadded:: 3.2

        .. versionchanged:: 4.0

           Added the ``code`` and ``reason`` arguments.r�  h9�h:M>K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	j?  u}r�  (hAh h	hThNj  u}r�  (hAj  h	hThNj  u�r�  hQNuauuj�  }r�  (hhh}r�  h	hTsuX   connect_futurer�  }r�  (hhh}r�  h	h"suX   protocolr�  }r�  (hj*  h}r�  h}r�  (hhh}r�  h	hTsu}r�  (hhh}r�  h	hssu�r�  suX   read_futurer�  }r�  (hj*  h}r�  h}r�  (hhh}r�  h	hTsu}r�  (hhh}r�  h	h"su�r�  suX
   read_queuer�  }r�  (hhh}r�  h	X   _collectionsr�  X   dequer�  �r�  suX   keyr�  }r�  (hhh}r�  h	NsuX   _on_message_callbackr�  }r�  (hhh}r�  h	hTsuj1  }r�  (hhh}r�  h	hTsuj4  }r�  (hj*  h}r�  h(}r�  (hhh}r�  h	hTsu}r�  (hhh}r�  h	h/su}r�  (hhh}r�  h	hGsu}r�  (hhh}r�  h	j@  sutr�  suh�}r�  (hhh}r�  h	hTsuh�}r�  (hhh}r�  h	hTsuht}r�  (hhh}r�  h	hTsuX
   tcp_clientr�  }r�  (hhh}r�  h	X   tornado.tcpclientr�  X	   TCPClientr�  �r�  suX   headersr�  }r�  (hhh}r�  h	hTsuX   _timeoutr�  }r�  (hhh}r�  h	hTsuX   io_loopr�  }r�  (hj*  h}r�  h}r�  (hhh}r�  h	h/su}r�  (hhh}r�  h	hTsu�r�  suX
   connectionr�  }r�  (hhh}r�  h	X   tornado.http1connectionr   X   HTTP1Connectionr  �r  suh�}r  (hj*  h}r  h}r  (hhh}r  h	hTsu}r  (hhh}r  h	X   tornado.iostreamr	  X   SSLIOStreamr
  �r  su}r  (hhh}r  h	j	  X   IOStreamr  �r  su�r  suX   final_callbackr  }r  (hj*  h}r  h}r  (hX   methodr  h}r  (h7Nh9�h:MUK	�r  h<]r  }r  (h?}r  (hAhBh	j?  u}r  (hAj�  h	Nu�r  hQNuaX   boundr  �uu}r  (hhh}r  h	hTsu�r   suX   requestr!  }r"  (hj*  h}r#  h}r$  (hhh}r%  h	j�  su}r&  (hhh}r'  h	jx  su�r(  suX
   start_timer)  }r*  (hhh}r+  h	h-X   floatr,  �r-  suX   clientr.  }r/  (hhh}r0  h	hTsuX   release_callbackr1  }r2  (hj*  h}r3  h}r4  (hhh}r5  h	hTsu}r6  (hh5h}r7  (h7Nh9�h:KK�r8  h<]r9  }r:  (h?)hQhTuauu}r;  (hh5h}r<  (h7Nh9�h:KK�r=  h<]r>  }r?  (h?)hQhTuauu�r@  suX   max_buffer_sizerA  }rB  (hhh}rC  h	jY  suX   max_header_sizerD  }rE  (hhh}rF  h	jY  suX   max_body_sizerG  }rH  (hhh}rI  h	jY  suX   coderJ  }rK  (hhh}rL  h	hTsuX   chunksrM  }rN  (hj*  h}rO  h}rP  (hhh}rQ  h	h�su}rR  (hhh}rS  h	h�su�rT  suX   _decompressorrU  }rV  (hhh}rW  h	hTsuX	   _sockaddrrX  }rY  (hhh}rZ  h	hTsuX   parsedr[  }r\  (hj*  h}r]  h}r^  (hhh}r_  h	h/su}r`  (hhh}ra  h	hTsu�rb  suX   parsed_hostnamerc  }rd  (hhh}re  h	NsuX   reasonrf  }rg  (hhh}rh  h	Nsuuh7X�   WebSocket client connection.

    This class should not be instantiated directly; use the
    `websocket_connect` function instead.ri  h9�h:MK�rj  uuX   WebSocketProtocolrk  }rl  (hh	h}rm  (h&]rn  (h(jk  �ro  h/eh0]rp  h/ah}rq  (h�}rr  (hh5h}rs  (h7Nh9�h:M�K	�rt  h<]ru  }rv  (h?}rw  (hAhBh	]rx  (jo  hseu}ry  (hAX   handlerrz  h	]r{  (h)j?  eu�r|  hQNuauuX   _run_callbackr}  }r~  (hh5h}r  (h7X�   Runs the given callback with exception handling.

        If the callback is a coroutine, returns its Future. On error, aborts the
        websocket connection and returns None.r�  h9�h:M�K	�r�  h<]r�  }r�  (h?(}r�  (hAhBh	]r�  (jo  hseu}r�  (hAj�  h	]r�  u}r�  (h^h_hAh`h	]r�  (h-hb]r�  ]r�  (hThGea�r�  heh-hb]r�  ]r�  (h/hGea�r�  eu}r�  (h^hghAhhh	hIutr�  hQ]r�  (h"X   tornado.genr�  X   MultiYieldPointr�  �r�  h/jg  hThIeuauuj  }r�  (hh5h}r�  (h7Nh9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	]r�  (jo  hseu�r�  hQNuauuX   _abortr�  }r�  (hh5h}r�  (h7X?   Instantly aborts the WebSocket connection by closing the socketr�  h9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	]r�  (hsjo  eu�r�  hQNuauuX   handlerr�  }r�  (hhh}r�  h	Nsuj!  }r�  (hhh}r�  h	Nsuh�}r�  (hhh}r�  h	NsuX   client_terminatedr�  }r�  (hhh}r�  h	hMsuX   server_terminatedr�  }r�  (hhh}r�  h	hMsuuh7X+   Base class for WebSocket protocol versions.r�  h9�h:M�K�r�  uuX   _websocket_maskr�  }r�  (hj*  h}r�  h}r�  (hhh}r�  hX#   tornado.util._websocket_mask_pythonr�  su}r�  (hhh}r�  hX   tornado.speedups.websocket_maskr�  su�r�  suX   WebSocketClosedErrorr�  }r�  (hh	h}r�  (h&]r�  (h(j�  �r�  jw  jz  j|  h/j}  eh0]r�  jw  ah}r�  h7XG   Raised by operations on a closed connection.

    .. versionadded:: 3.2r�  h9�h:K4K�r�  uuX   PeriodicCallbackr�  }r�  (hhh]r�  j�  X   PeriodicCallbackr�  �r�  auX   simple_httpclientr�  }r�  (hhhj@  h�r�  uX   urlparser�  }r�  (hj*  h}r�  h}r�  (hhh}r�  hX   urllib.parse.urlparser�  su}r�  (hhh}r�  hX   urlparse.urlparser�  su�r�  suX   hashlibr�  }r�  (hhhX   hashlibr�  h�r�  uhr}r�  (hh	h}r�  (h&]r�  (hsjo  h/eh0]r�  jo  ah}r�  (X   RSV3r�  }r�  (hhh}r�  h	jY  suX   _handle_websocket_headersr�  }r�  (hh5h}r�  (h7X�   Verifies all invariant- and required headers

        If a header is missing or have an incorrect value ValueError will be
        raisedr�  h9�h:MzK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu�r�  hQNuauuh3}r�  (hh5h}r�  (h7X9   Sends the given message to the client of this Web Socket.r�  h9�h:MK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAhDh	]r�  (h/hGhIeu}r�  (hAhKh	hMhNhOu�r�  hQ]r�  (h"hTeuauuh�}r�  (hhvh}r�  (h7Nh	]r�  (jY  hTeh:M�K	�r   uuX   _receive_framer  }r  (hh5h}r  (h7Nh9�h:M K	�r  h<]r  }r  (h?}r  (hAhBh	hsu�r  hQNuauuX   _accept_connectionr	  }r
  (hh5h}r  (h7Nh9�h:M�K	�r  h<]r  }r  (h?}r  (hAhBh	hsu�r  hQNuauuX   compute_accept_valuer  }r  (hh5h}r  (h7Xf   Computes the value for the Sec-WebSocket-Accept header,
        given the value for Sec-WebSocket-Key.r  h9�h:M�K	�r  h<]r  }r  (h?}r  (hAX   keyr  h	hTu�r  hQ]r  (h/j@  hGeuaX   staticr  �uuX   _challenge_responser  }r  (hh5h}r  (h7Nh9�h:M�K	�r   h<]r!  }r"  (h?}r#  (hAhBh	hsu�r$  hQ]r%  (h/j@  hGeuauuX   _write_framer&  }r'  (hh5h}r(  (h7Nh9�h:M�K	�r)  h<]r*  }r+  (h?(}r,  (hAhBh	hsu}r-  (hAX   finr.  h	hMu}r/  (hAX   opcoder0  h	jY  u}r1  (hAh�h	]r2  (hGhTeu}r3  (hAX   flagsr4  h	jY  hNX   0r5  utr6  hQ]r7  (h"hTeuauuX   _on_frame_datar8  }r9  (hh5h}r:  (h7Nh9�h:MuK	�r;  h<]r<  }r=  (h?}r>  (hAhBh	hsu}r?  (hAh�h	]r@  (hGhTeu�rA  hQNuauuX   _on_frame_length_64rB  }rC  (hh5h}rD  (h7Nh9�h:M^K	�rE  h<]rF  }rG  (h?}rH  (hAhBh	hsu}rI  (hAh�h	Nu�rJ  hQNuauuh�}rK  (hhvh}rL  (h7Nh	]rM  (jY  hTeh:M�K	�rN  uuh�}rO  (hh5h}rP  (h7Nh9�h:MKK	�rQ  h<]rR  }rS  (h?(}rT  (hAhBh	hsu}rU  (hAjz  h	]rV  (h)j?  eu}rW  (hAX   mask_outgoingrX  h	hMhNhOu}rY  (hAj�  h	hThNj  utrZ  hQNuauuX   _read_frame_datar[  }r\  (hh5h}r]  (h7Nh9�h:MHK	�r^  h<]r_  }r`  (h?}ra  (hAhBh	hsu}rb  (hAX   maskedrc  h	hMu�rd  hQNuauuX   _process_server_headersre  }rf  (hh5h}rg  (h7X�   Process the headers sent by the server to this client connection.

        'key' is the websocket handshake challenge/response key.rh  h9�h:M�K	�ri  h<]rj  }rk  (h?}rl  (hAhBh	hsu}rm  (hAj  h	Nu}rn  (hAjc  h	hTu�ro  hQNuauuX   RSV_MASKrp  }rq  (hhh}rr  h	jY  suX   OPCODE_MASKrs  }rt  (hhh}ru  h	jY  suX
   write_pingrv  }rw  (hh5h}rx  (h7X   Send ping frame.ry  h9�h:MK	�rz  h<]r{  }r|  (h?}r}  (hAhBh	hsu}r~  (hAh�h	hGu�r  hQNuauuX   _get_compressor_optionsr�  }r�  (hh5h}r�  (h7Xc   Converts a websocket agreed_parameters set to keyword arguments
        for our compressor objects.r�  h9�h:M�K	�r�  h<]r�  }r�  (h?(}r�  (hAhBh	hsu}r�  (hAX   sider�  h	hGu}r�  (hAX   agreed_parametersr�  h	hIu}r�  (hAj�  h	hThNj  utr�  hQhIuauuX   _on_masking_keyr�  }r�  (hh5h}r�  (h7Nh9�h:MiK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAh�h	Nu�r�  hQNuauuX   _create_compressorsr�  }r�  (hh5h}r�  (h7Nh9�h:M�K	�r�  h<]r�  }r�  (h?(}r�  (hAhBh	hsu}r�  (hAj�  h	hGu}r�  (hAj�  h	hIu}r�  (hAj�  h	hThNj  utr�  hQNuauuX   RSV1r�  }r�  (hhh}r�  h	jY  suX   _handle_messager�  }r�  (hh5h}r�  (h7X>   Execute on_message, returning its Future if it is a coroutine.r�  h9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAj0  h	hTu}r�  (hAh�h	]r�  (hGhTeu�r�  hQ]r�  (h"j�  h/jg  hThIeuauuX   _on_frame_startr�  }r�  (hh5h}r�  (h7Nh9�h:M&K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAh�h	Nu�r�  hQNuauuX   _parse_extensions_headerr�  }r�  (hh5h}r�  (h7Nh9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAjc  h	]r�  (hTjC  X   HTTPHeadersr�  �r�  eu�r�  hQ]r�  (h-h�]r�  h-hb]r�  (NhIe�r�  a�r�  h�jg  euauuh�}r�  (hh5h}r�  (h7X    Closes the WebSocket connection.r�  h9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAh h	]r�  (jY  hTehNj  u}r�  (hAj  h	]r�  (hGhTehNj  u�r�  hQNuauuX   FINr�  }r�  (hhh}r�  h	jY  suX   RSV2r�  }r�  (hhh}r�  h	jY  suX   _on_masked_frame_datar�  }r�  (hh5h}r�  (h7Nh9�h:MqK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAh�h	Nu�r�  hQNuauuX   accept_connectionr�  }r�  (hh5h}r�  (h7Nh9�h:MhK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu�r�  hQNuauuX   _on_frame_length_16r�  }r�  (hh5h}r�  (h7Nh9�h:MSK	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu}r�  (hAh�h	Nu�r�  hQNuauuX   start_pingingr�  }r�  (hh5h}r�  (h7X9   Start sending periodic pings to keep the connection aliver�  h9�h:M�K	�r�  h<]r�  }r�  (h?}r�  (hAhBh	hsu�r   hQNuauuX   periodic_pingr  }r  (hh5h}r  (h7Xx   Send a ping to keep the websocket alive

        Called periodically if the websocket_ping_interval is set and non-zero.r  h9�h:M�K	�r  h<]r  }r  (h?}r  (hAhBh	hsu�r	  hQNuauujX  }r
  (hhh}r  h	hMsuX   _final_framer  }r  (hhh}r  h	hMsuX   _frame_opcoder  }r  (hhh}r  h	hTsuX   _masked_framer  }r  (hj*  h}r  h}r  (hhh}r  h	hTsu}r  (hhh}r  h	hMsu�r  suX   _frame_maskr  }r  (hhh}r  h	hTsuX   _frame_lengthr  }r  (hhh}r  h	hTsuX   _fragmented_message_bufferr   }r!  (hj*  h}r"  h}r#  (hhh}r$  h	hTsu}r%  (hhh}r&  h	hGsu�r'  suX   _fragmented_message_opcoder(  }r)  (hhh}r*  h	hTsuX   _waitingr+  }r,  (hhh}r-  h	hTsuX   _compression_optionsr.  }r/  (hhh}r0  h	hTsuj  }r1  (hj*  h}r2  h}r3  (hhh}r4  h	hTsu}r5  (hhh}r6  h	j�  su�r7  suj�  }r8  (hj*  h}r9  h}r:  (hhh}r;  h	hTsu}r<  (hhh}r=  h	j�  su�r>  suX   _frame_compressedr?  }r@  (hj*  h}rA  h}rB  (hhh}rC  h	hTsu}rD  (hhh}rE  h	hMsu�rF  suX   _message_bytes_inrG  }rH  (hhh}rI  h	jY  suX   _message_bytes_outrJ  }rK  (hhh}rL  h	jY  suX   _wire_bytes_inrM  }rN  (hhh}rO  h	jY  suX   _wire_bytes_outrP  }rQ  (hhh}rR  h	jY  suX   ping_callbackrS  }rT  (hj*  h}rU  h}rV  (hhh}rW  h	hTsu}rX  (hhh}rY  h	j�  su�rZ  suX	   last_pingr[  }r\  (hj*  h}r]  h}r^  (hhh}r_  h	jY  su}r`  (hhh}ra  h	j-  su�rb  suX	   last_pongrc  }rd  (hj*  h}re  h}rf  (hhh}rg  h	jY  su}rh  (hhh}ri  h	j-  su�rj  suj�  }rk  (hj*  h}rl  h}rm  (hhh}rn  h	h)su}ro  (hhh}rp  h	j?  su�rq  suj!  }rr  (hj*  h}rs  h}rt  (hhh}ru  h	jx  su}rv  (hhh}rw  h	j�  su�rx  suh�}ry  (hj*  h}rz  h(}r{  (hhh}r|  h	hTsu}r}  (hhh}r~  h	hTsu}r  (hhh}r�  h	j  su}r�  (hhh}r�  h	j  sutr�  suX   _frame_opcode_is_controlr�  }r�  (hhh}r�  h	jY  suj�  }r�  (hhh}r�  h	hMsuj�  }r�  (hhh}r�  h	hMsuuh7X�   Implementation of the WebSocket protocol from RFC 6455.

    This class supports versions 7 and 8 of the protocol in addition to the
    final version 13.r�  h9�h:M=K�r�  uuX   app_logr�  }r�  (hj*  h}r�  h(}r�  (hhh}r�  h	hTsu}r�  (hhh}r�  h	h/su}r�  (hhh}r�  h	j1  su}r�  (hhh}r�  h	j5  su}r�  (hhh}r�  h	j9  sutr�  suX   PY3r�  }r�  (hhh}r�  h	NsuX	   TCPClientr�  }r�  (hhh]r�  j�  auX   StreamClosedErrorr�  }r�  (hhh]r�  j	  X   StreamClosedErrorr�  �r�  auX   IOLoopr�  }r�  (hhh]r�  j�  auX#   _raise_not_supported_for_websocketsr�  }r�  (hh5h}r�  (h7Nh9�h:M�K�r�  h<]r�  }r�  (h?}r�  (h^h_hAh`h	heu}r�  (h^hghAhhh	hIu�r�  hQNuauuX
   native_strr�  }r�  (hj*  h}r�  h}r�  (hhh}r�  hj�  su}r�  (hhh}r�  hhsu�r�  suX   osr�  }r�  (hhhX   osr�  h�r�  uX   xranger�  }r�  (hhh}r�  hX   __builtin__.ranger�  suX   genr�  }r�  (hhhj�  h�r�  uX   base64r�  }r�  (hhhX   base64r�  h�r�  uX   absolute_importr�  }r�  (hhh}r�  h	hsuX   httputilr�  }r�  (hhhjC  h�r�  uX   tornador�  }r�  (hhhX   tornador�  h�r�  uuh7X�  Implementation of the WebSocket protocol.

`WebSockets <http://dev.w3.org/html5/websockets/>`_ allow for bidirectional
communication between the browser and server.

WebSockets are supported in the current versions of all major browsers,
although older versions that do not support WebSockets are still in use
(refer to http://caniuse.com/websockets for details).

This module implements the final version of the WebSocket protocol as
defined in `RFC 6455 <http://tools.ietf.org/html/rfc6455>`_.  Certain
browser versions (notably Safari 5.x) implemented an earlier draft of
the protocol (known as "draft 76") and are not compatible with this module.

.. versionchanged:: 4.0
   Removed support for the draft 76 protocol version.r�  X   childrenr�  ]r�  X   filenamer�  X   c:\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\lib\site-packages\tornado\websocket.pyr�  u.