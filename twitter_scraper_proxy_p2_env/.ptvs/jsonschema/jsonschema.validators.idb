�}q (X   membersq}q(X   UnknownTypeq}q(X   kindqX   typerefqX   valueq]qX   jsonschema.exceptionsq	X   UnknownTypeq
�qauX	   lru_cacheq}q(hX   funcrefqh}qX	   func_nameqX!   functools32.functools32.lru_cacheqsuX   urlopenq}q(hX   multipleqh}qh}q(hhh}qhX   urllib.request.urlopenqsu}q(hhh}qhX   urllib2.urlopenqsu�qsuX   requestsq}q(hX   dataqh}q X   typeq!X   __builtin__q"X   NoneTypeq#�q$suX   unquoteq%}q&(hhh}q'h}q((hhh}q)hX   urllib.parse.unquoteq*su}q+(hhh}q,hX   urllib.unquoteq-su�q.suX   Sequenceq/}q0(hhh]q1X   _abcollq2X   Sequenceq3�q4auX	   validatesq5}q6(hX   functionq7h}q8(X   docq9X  
    Register the decorated validator for a ``version`` of the specification.

    Registered validators and their meta schemas will be considered when
    parsing ``$schema`` properties' URIs.

    Arguments:

        version (str):

            An identifier to use as the version's name

    Returns:

        callable: a class decorator to decorate the validator with the versionq:X   builtinq;�X   locationq<KK�q=X	   overloadsq>]q?}q@(X   argsqA}qB(X   nameqCX   versionqDh!]qE(h$h"X   strqF�qGeu�qHX   ret_typeqI]qJh"h7�qKauauuX	   ErrorTreeqL}qM(hhh]qNh	X	   ErrorTreeqO�qPauX   numbersqQ}qR(hX	   modulerefqShX   numbersqTX    qU�qVuX   validateqW}qX(hh7h}qY(h9Xq  
    Validate an instance under the given schema.

        >>> validate([2, 3, 4], {"maxItems": 2})
        Traceback (most recent call last):
            ...
        ValidationError: [2, 3, 4] is too long

    :func:`validate` will first verify that the provided schema is itself
    valid, since not doing so can lead to less obvious error messages and fail
    in less obvious or consistent ways. If you know you have a valid schema
    already or don't care, you might prefer using the
    :meth:`~IValidator.validate` method directly on a specific validator
    (e.g. :meth:`Draft4Validator.validate`).


    Arguments:

        instance:

            The instance to validate

        schema:

            The schema to validate with

        cls (:class:`IValidator`):

            The class that will be used to validate the instance.

    If the ``cls`` argument is not provided, two things will happen in
    accordance with the specification. First, if the schema has a
    :validator:`$schema` property containing a known meta-schema [#]_ then the
    proper validator will be used.  The specification recommends that all
    schemas contain :validator:`$schema` properties for this reason. If no
    :validator:`$schema` property is found, the default validator class is
    :class:`Draft4Validator`.

    Any other provided positional and keyword arguments will be passed on when
    instantiating the ``cls``.

    Raises:

        :exc:`ValidationError` if the instance is invalid

        :exc:`SchemaError` if the schema itself is invalid

    .. rubric:: Footnotes
    .. [#] known by a validator registered with :func:`validates`qZh;�h<M�K�q[h>]q\}q](hA(}q^(hCX   instanceq_h!Nu}q`(hCX   schemaqah!Nu}qb(hCX   clsqch!h$X   default_valueqdX   Noneqeu}qf(X
   arg_formatqgX   *qhhCX   argsqih!h"X   tupleqj�qku}ql(hgX   **qmhCX   kwargsqnh!h"X   dictqo�qputqqhINuauuX   validator_forqr}qs(hh7h}qt(h9Nh;�h<M�K�quh>]qv}qw(hA}qx(hChah!]qy(h$h"X   objectqz�q{eu}q|(hCX   defaultq}h!]q~(X   jsonschema.validatorsqX	   Validatorq��q�X   jsonschema._utilsq�X   Unsetq��q�ehdX   _unsetq�u�q�hIh$uauuX   SchemaErrorq�}q�(hhh]q�h	X   SchemaErrorq��q�auX   extendq�}q�(hh7h}q�(h9Nh;�h<K�K�q�h>]q�}q�(hA}q�(hCX	   validatorq�h!Nu}q�(hCX
   validatorsq�h!Nu}q�(hChDh!h$hdheu�q�hIh�uauuX   meta_schemasq�}q�(hhh}q�h!h�X   URIDictq��q�suX	   int_typesq�}q�(hhh}q�h}q�(hhh}q�h!h"hj]q�h"X   intq��q�a�q�su}q�(hhh}q�h!h"hj]q�(h�h"X   longq��q�e�q�su�q�suh�}q�(hhh}q�h!h�suX	   urldefragq�}q�(hhh}q�hX   jsonschema.compat.urldefragq�suX   RefResolutionErrorq�}q�(hhh]q�h	X   RefResolutionErrorq��q�auX	   iteritemsq�}q�(hhh}q�h!X   operatorq�X   methodcallerq��q�suX   urlsplitq�}q�(hhh}q�hX   jsonschema.compat.urlsplitq�suX   _utilsq�}q�(hhShh�hU�q�uX   divisionq�}q�(hhh}q�h!X
   __future__q�X   _FeatureqɆq�suX   RefResolverq�}q�(hh!h}q�(X   mroq�]q�(hhˆq�h{eX   basesq�]q�h{ah}q�(X	   resolvingq�}q�(hhh}q�h!hKsuX   resolution_scopeq�}q�(hX   propertyq�h}q�(h9Nh!h"X   unicodeqۆq�h<MPK	�q�uuX   resolve_remoteq�}q�(hh7h}q�(h9X�  
        Resolve a remote ``uri``.

        If called directly, does not check the store first, but after
        retrieving the document at the specified URI it will be saved in
        the store if :attr:`cache_remote` is True.

        .. note::

            If the requests_ library is present, ``jsonschema`` will use it to
            request the remote ``uri``, so that the correct encoding is
            detected and used.

            If it isn't, or if the scheme of the ``uri`` is not ``http`` or
            ``https``, UTF-8 is assumed.

        Arguments:

            uri (str):

                The URI to resolve

        Returns:

            The retrieved document

        .. _requests: http://pypi.python.org/pypi/requests/q�h;�h<M�K	�q�h>]q�}q�(hA}q�(hCX   selfq�h!h�u}q�(hCX   uriq�h!]q�(hKh"X   floatq�q�h"X   boolq�q�h{h$hGX	   cookielibq�X   Absentq�q�h�h�eu�q�hI]q�(h�h{h$h�h�euauuX   __init__q�}q�(hh7h}q�(h9Nh;�h<MK	�q�h>]q�}q�(hA(}q�(hCh�h!h�u}q�(hCX   base_uriq�h!h�u}q�(hCX   referrerq�h!hpu}q�(hCX   storeq�h!hkhdX   ()q u}r  (hCX   cache_remoter  h!h�hdX   Truer  u}r  (hCX   handlersr  h!hkhdh u}r  (hCX   urljoin_cacher  h!]r  (h$h{ehdheu}r	  (hCX   remote_cacher
  h!]r  (h$h{ehdheutr  hINuauuh�}r  (hh�h}r  (h9Nh!]r  (hKh�h�h{h$hGh�h�h�eh<MTK	�r  uuX   resolver  }r  (hh7h}r  (h9Nh;�h<MuK	�r  h>]r  }r  (hA}r  (hCh�h!h�u}r  (hCX   refr  h!]r  (h"X   listr  ]r  Na�r  h"j  ]r  ]r  (hGh"X
   basestringr   �r!  ea�r"  h{h"j  �r#  eu�r$  hI]r%  h"hj]r&  (NNe�r'  auauuX   from_schemar(  }r)  (hh7h}r*  (h9X�   
        Construct a resolver from a JSON schema object.

        Arguments:

            schema:

                the referring schema

        Returns:

            :class:`RefResolver`r+  h;�h<M.K	�r,  h>]r-  }r.  (hA(}r/  (hChch!h�u}r0  (hChah!hpu}r1  (hghhhChih!]r2  (h"hj]r3  Na�r4  hkeu}r5  (hghmhChnh!hputr6  hIh�uaX   classmethodr7  �uuX   in_scoper8  }r9  (hhh}r:  h!hKsuX
   push_scoper;  }r<  (hh7h}r=  (h9Nh;�h<M@K	�r>  h>]r?  }r@  (hA}rA  (hCh�h!h�u}rB  (hCX   scoperC  h!]rD  (j  j"  eu�rE  hINuauuX   resolve_fragmentrF  }rG  (hh7h}rH  (h9X�   
        Resolve a ``fragment`` within the referenced ``document``.

        Arguments:

            document:

                The referrant document

            fragment (str):

                a URI fragment to resolve within itrI  h;�h<M�K	�rJ  h>]rK  }rL  (hA}rM  (hCh�h!h�u}rN  (hCX   documentrO  h!]rP  (h�h{h$h4h�h�eu}rQ  (hCX   fragmentrR  h!hGu�rS  hI]rT  (h�h{h$h4h�h�euauuX   resolve_from_urlrU  }rV  (hh7h}rW  (h9Nh;�h<MyK	�rX  h>]rY  }rZ  (hA}r[  (hCh�h!h�u}r\  (hCX   urlr]  h!]r^  (hKh�h�h{h$hGh�h�h�eu�r_  hI]r`  (h�h{h$h4h�h�euauuX	   pop_scopera  }rb  (hh7h}rc  (h9Nh;�h<MEK	�rd  h>]re  }rf  (hA}rg  (hCh�h!h�u�rh  hINuauuh�}ri  (hhh}rj  h(}rk  (hhh}rl  h!hpsu}rm  (hhh}rn  h!hpsu}ro  (hhh}rp  h!hpsu}rq  (hhh}rr  h!hpsutrs  suj  }rt  (hhh}ru  h!h�suj  }rv  (hhh}rw  h}rx  (hhh}ry  h!hpsu}rz  (hhh}r{  h!hpsu}r|  (hhh}r}  h!hpsu�r~  suX   _scopes_stackr  }r�  (hhh}r�  h}r�  (hhh}r�  h!j  su}r�  (hhh}r�  h!j  su}r�  (hhh}r�  h!h"j  ]r�  h�a�r�  su�r�  suh�}r�  (hhh}r�  h!h�suX   _urljoin_cacher�  }r�  (hhh}r�  h}r�  (hhh}r�  h!h$su}r�  (hhh}r�  h!h{su�r�  suX   _remote_cacher�  }r�  (hhh}r�  h}r�  (hhh}r�  h!h$su}r�  (hhh}r�  h!h{su�r�  suuh9X   
    Resolve JSON References.

    Arguments:

        base_uri (str):

            The URI of the referring document

        referrer:

            The actual referring document

        store (dict):

            A mapping from URIs to documents to cache

        cache_remote (bool):

            Whether remote refs should be cached after first resolution

        handlers (dict):

            A mapping from URI schemes to functions that should be used
            to retrieve them

        urljoin_cache (functools.lru_cache):

            A cache that will be used for caching the results of joining
            the resolution scope to subscopes.

        remote_cache (functools.lru_cache):

            A cache that will be used for caching the results of
            resolved remote URLs.r�  h;�h<K�K�r�  uuX   Draft4Validatorr�  }r�  (hh!h}r�  (h�]r�  (h�h{eh�]r�  h{ah}r�  (X   DEFAULT_TYPESr�  }r�  (hhh}r�  h!hpsuh�}r�  (hh7h}r�  (h9Nh;�h<KCK�r�  h>]r�  }r�  (hA(}r�  (hCh�h!h�u}r�  (hChah!hpu}r�  (hCX   typesr�  h!hkhdh u}r�  (hCX   resolverr�  h!]r�  (h$h�ehdheu}r�  (hCX   format_checkerr�  h!h$hdheutr�  hINuauuX
   VALIDATORSr�  }r�  (hhh}r�  h!hpsuX   META_SCHEMAr�  }r�  (hhh}r�  h!hpsuX   descendr�  }r�  (hh7h}r�  (h9Nh;�h<KxK�r�  h>]r�  }r�  (hA(}r�  (hCh�h!h�u}r�  (hCh_h!Nu}r�  (hChah!Nu}r�  (hCX   pathr�  h!h$hdheu}r�  (hCX   schema_pathr�  h!h$hdheutr�  hIh"X	   generatorr�  �r�  uauuhW}r�  (hh7h}r�  (h9Nh;�h<K�K�r�  h>]r�  }r�  (hA}r�  (hCh�h!h�u}r�  (hghhhChih!hku}r�  (hghmhChnh!hpu�r�  hINuauuX   __name__r�  }r�  (hhh}r�  h!hGsuX   is_validr�  }r�  (hh7h}r�  (h9Nh;�h<K�K�r�  h>]r�  }r�  (hA}r�  (hCh�h!h�u}r�  (hCh_h!Nu}r�  (hCX   _schemar�  h!h$hdheu�r�  hINuauuX   is_typer�  }r�  (hh7h}r�  (h9Nh;�h<K�K�r�  h>]r�  }r�  (hA}r�  (hCh�h!h�u}r�  (hCh_h!h�u}r�  (hCX   typer�  h!Nu�r�  hIh�uauuX   check_schemar�  }r�  (hh7h}r�  (h9Nh;�h<KQK�r�  h>]r�  }r�  (hA}r�  (hChch!h�u}r�  (hChah!Nu�r�  hINuaj7  �uuX   iter_errorsr�  }r�  (hh7h}r�  (h9Nh;�h<KUK�r�  h>]r�  }r�  (hA}r�  (hCh�h!h�u}r�  (hCh_h!Nu}r   (hCj�  h!]r  (h$hpehdheu�r  hI]r  j�  auauuX   _typesr  }r  (hhh}r  h}r  (hhh}r  h!hpsu}r	  (hhh}r
  h!hpsu�r  suj�  }r  (hhh}r  h}r  (hhh}r  h!h$su}r  (hhh}r  h!h�su�r  suj�  }r  (hhh}r  h!h$suha}r  (hhh}r  h!hpsuuh9Nh;�h<K>K�r  uuX
   contextlibr  }r  (hhShX
   contextlibr  hU�r  uX   jsonr  }r  (hhShX   jsonr  hU�r  uX   Draft3Validatorr   }r!  (hh!h}r"  (h�]r#  (h�h{eh�]r$  h{ah}r%  (j�  }r&  (hhh}r'  h!hpsuh�}r(  (hh7h}r)  (h9Nh;�h<KCK�r*  h>]r+  }r,  (hA(}r-  (hCh�h!h�u}r.  (hChah!hpu}r/  (hCj�  h!hkhdh u}r0  (hCj�  h!]r1  (h$h�ehdheu}r2  (hCj�  h!h$hdheutr3  hINuauuj�  }r4  (hhh}r5  h!hpsuj�  }r6  (hhh}r7  h!hpsuj�  }r8  (hh7h}r9  (h9Nh;�h<KxK�r:  h>]r;  }r<  (hA(}r=  (hCh�h!h�u}r>  (hCh_h!Nu}r?  (hChah!]r@  (j!  j"  hGj  h{j#  eu}rA  (hCj�  h!h$hdheu}rB  (hCj�  h!h$hdheutrC  hI]rD  j�  auauuhW}rE  (hh7h}rF  (h9Nh;�h<K�K�rG  h>]rH  }rI  (hA}rJ  (hCh�h!h�u}rK  (hghhhChih!hku}rL  (hghmhChnh!hpu�rM  hINuauuj�  }rN  (hhh}rO  h!hGsuj�  }rP  (hh7h}rQ  (h9Nh;�h<K�K�rR  h>]rS  }rT  (hA}rU  (hCh�h!h�u}rV  (hCh_h!Nu}rW  (hCj�  h!]rX  (h$hpehdheu�rY  hIh�uauuj�  }rZ  (hh7h}r[  (h9Nh;�h<K�K�r\  h>]r]  }r^  (hA}r_  (hCh�h!h�u}r`  (hCh_h!]ra  (hpj"  h{hGj!  h�j  j#  eu}rb  (hCj�  h!hGu�rc  hIh�uauuj�  }rd  (hh7h}re  (h9Nh;�h<KQK�rf  h>]rg  }rh  (hA}ri  (hChch!h�u}rj  (hChah!Nu�rk  hINuaj7  �uuj�  }rl  (hh7h}rm  (h9Nh;�h<KUK�rn  h>]ro  }rp  (hA}rq  (hCh�h!h�u}rr  (hCh_h!Nu}rs  (hCj�  h!]rt  (hpj!  j"  h{h$j  j#  hGehdheu�ru  hI]rv  j�  auauuj  }rw  (hhh}rx  h}ry  (hhh}rz  h!hpsu}r{  (hhh}r|  h!hpsu�r}  suj�  }r~  (hhh}r  h}r�  (hhh}r�  h!h$su}r�  (hhh}r�  h!h�su�r�  suj�  }r�  (hhh}r�  h!h$suha}r�  (hhh}r�  h!hpsuuh9Nh;�h<K>K�r�  uuX   creater�  }r�  (hh7h}r�  (h9Nh;�h<K6K�r�  h>]r�  }r�  (hA(}r�  (hCX   meta_schemar�  h!]r�  (h�h{h$h�h�eu}r�  (hCh�h!]r�  (hkhpehdh u}r�  (hChDh!]r�  (h$hGehdheu}r�  (hCX   default_typesr�  h!]r�  (h$hpehdheutr�  hI]r�  h�auauuX   urljoinr�  }r�  (hhh}r�  h}r�  (hhh}r�  hX   urllib.parse.urljoinr�  su}r�  (hhh}r�  hX   urlparse.urljoinr�  su�r�  suX	   str_typesr�  }r�  (hhh}r�  h}r�  (hhh}r�  h!h"hj]r�  hGa�r�  su}r�  (hhh]r�  j!  au�r�  suh�}r�  (hhh}r�  h!hpsuX   _validatorsr�  }r�  (hhShX   jsonschema._validatorsr�  hU�r�  uuh9hUX   childrenr�  ]r�  X   filenamer�  X�   c:\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\lib\site-packages\jsonschema\validators.pyr�  u.