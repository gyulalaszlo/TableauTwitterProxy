�}q (X   membersq}q(X   UEMPTYSTRINGq}q(X   kindqX   dataqX   valueq}qX   typeq	X   __builtin__q
X   unicodeq�qsuX	   _binsplitq}q(hX   functionqh}q(X   docqNX   builtinq�X   locationqM�K�qX	   overloadsq]q}q(X   argsq}q(X   nameqX
   splittableqh	]q(h
X   NoneTypeq�qhh
X   strq�q X   email.headerq!X   Headerq"�q#h
X   objectq$�q%eu}q&(hX   charsetq'h	]q((X   email.charsetq)X   Charsetq*�q+heu}q,(hX
   maxlinelenq-h	]q.(h
X   intq/�q0hh%eu�q1X   ret_typeq2]q3(h
X   tupleq4]q5(NNe�q6h
h4]q7(]q8(h%h he]q9(h%h hee�q:euauuX   req;}q<(hX	   modulerefq=hX   req>X    q?�q@uX
   MAXLINELENqA}qB(hhh}qCh	h0suX   CharsetqD}qE(hX   typerefqFh]qGh+auX   USASCIIqH}qI(hhh}qJh	h+suX   fcreqK}qL(hX   multipleqMh}qNh}qO(hhh}qPh	h%su}qQ(hhh}qRh	X   _sreqSX   SRE_PatternqT�qUsu�qVsuX   SPACEqW}qX(hhh}qYh	h suX   binasciiqZ}q[(hh=hX   binasciiq\h?�q]uX   emailq^}q_(hh=hX   emailq`h?�qauX   __all__qb}qc(hhh}qdh	h
X   listqe]qf(h h h e�qgsuX   make_headerqh}qi(hhh}qj(hX�  Create a Header from a sequence of pairs as returned by decode_header()

    decode_header() takes a header value string and returns a sequence of
    pairs of the format (decoded_string, charset) where charset is the string
    name of the character set.

    This function takes one of those sequence of pairs and returns a Header
    instance.  Optional maxlinelen, header_name, and continuation_ws are as in
    the Header constructor.qkh�hKrK�qlh]qm}qn(h(}qo(hX   decoded_seqqph	Nu}qq(hh-h	hX   default_valueqrX   Noneqsu}qt(hX   header_namequh	hhrhsu}qv(hX   continuation_wsqwh	h hrX   ' 'qxutqyh2h#uauuX   SPACE8qz}q{(hhh}q|h	h suX   USPACEq}}q~(hhh}qh	hsuX   _split_asciiq�}q�(hhh}q�(hNh�hM�K�q�h]q�}q�(h(}q�(hX   sq�h	]q�(hhh h#h%eu}q�(hX   firstlenq�h	]q�(h0hh%eu}q�(hX   restlenq�h	h0u}q�(hhwh	h u}q�(hX
   splitcharsq�h	]q�(hh h%eutq�h2]q�h
he]q�h a�q�auauuh"}q�(hh	h}q�(X   mroq�]q�h#aX   basesq�]q�h}q�(X   _splitq�}q�(hhh}q�(hNh�hMK	�q�h]q�}q�(h(}q�(hX   selfq�h	h#u}q�(hh�h	]q�(hhh h#h%eu}q�(hh'h	]q�(h+heu}q�(hh-h	]q�(h0hh%eu}q�(hh�h	]q�(hh h%eutq�h2]q�(h
he�q�h
he]q�]q�(X   multiprocessing.processq�X   Processq��q�X   multiprocessing.dummyq�X   DummyProcessq��q�ea�q�h
he]q�h
h4]q�(]q�(hh%h hh#e]q�(hh+ee�q�a�q�h
he]q�h6a�q�euauuX   encodeq�}q�(hhh}q�(hX~  Encode a message header into an RFC-compliant format.

        There are many issues involved in converting a given string for use in
        an email header.  Only certain character sets are readable in most
        email clients, and as header strings can only contain a subset of
        7-bit ASCII, care must be taken to properly convert and encode (with
        Base64 or quoted-printable) header strings.  In addition, there is a
        75-character length limit on any given encoded header field, so
        line-wrapping must be performed, even with double-byte character sets.

        This method will do its best to convert the string to the correct
        character set used in email, and encode and line wrap it safely with
        the appropriate scheme for that character set.

        If the given charset is not known or an error occurs during
        conversion, this function will return the header untouched.

        Optional splitchars is a string containing characters to split long
        ASCII lines on, in rough support of RFC 2822's `highest level
        syntactic breaks'.  This doesn't affect RFC 2047 encoded lines.q�h�hMoK	�q�h]q�}q�(h}q�(hh�h	h#u}q�(hh�h	]q�(hh h%ehrX   ';, 'q�u�q�h2h uauuX   __init__q�}q�(hhh}q�(hX  Create a MIME-compliant header that can contain many character sets.

        Optional s is the initial header value.  If None, the initial header
        value is not set.  You can later append to the header with .append()
        method calls.  s may be a byte string or a Unicode string, but see the
        .append() documentation for semantics.

        Optional charset serves two purposes: it has the same meaning as the
        charset argument to the .append() method.  It also sets the default
        character set for all subsequent .append() calls that omit the charset
        argument.  If charset is not provided in the constructor, the us-ascii
        charset is used both as s's initial charset and as the default for
        subsequent .append() calls.

        The maximum line length can be specified explicit via maxlinelen.  For
        splitting the first line to a shorter value (to account for the field
        header which isn't included in s, e.g. `Subject') pass in the name of
        the field in header_name.  The default maxlinelen is 76.

        continuation_ws must be RFC 2822 compliant folding whitespace (usually
        either a space or a hard tab) which will be prepended to continuation
        lines.

        errors is passed through to the .append() call.q�h�hK�K	�q�h]q�}q�(h(}q�(hh�h	h#u}q�(hh�h	]q�(hh h#ehrhsu}q�(hh'h	]q�(h+hehrhsu}q�(hh-h	]q�(h0hehrhsu}q�(hhuh	]q�(hh ehrhsu}q�(hhwh	h hrX   ' 'q�u}q�(hX   errorsq�h	h hrX   'strict'q�utq�h2Nuauuh�}q�(hhh}q�(hNh�hMGK	�q�h]q�}q�(h(}q�(hh�h	h#u}q�(hh�h	]q�(hhh h#h%eu}q�(hh'h	]q�(h+heu}q�(hh�h	]q�(h0hh%eu}q�(hh�h	]q�(hh h%eutq�h2h�uauuX   __eq__q�}q�(hhh}q�(hNh�hK�K	�q�h]q�}q�(h}q�(hh�h	h#u}q�(hX   otherq�h	Nu�q�h2NuauuX   appendq�}q�(hhh}q�(hX5  Append a string to the MIME header.

        Optional charset, if given, should be a Charset instance or the name
        of a character set (which will be converted to a Charset instance).  A
        value of None (the default) means that the charset given in the
        constructor is used.

        s may be a byte string or a Unicode string.  If it is a byte string
        (i.e. isinstance(s, str) is true), then charset is the encoding of
        that byte string, and a UnicodeError will be raised if the string
        cannot be decoded with that charset.  If s is a Unicode string, then
        charset is a hint specifying the character set of the characters in
        the string.  In this case, when producing an RFC 2822 compliant header
        using RFC 2047 rules, the Unicode string will be encoded using the
        following charsets in order: us-ascii, the charset hint, utf-8.  The
        first character set not to provoke a UnicodeError is used.

        Optional `errors' is passed as the third argument to any unicode() or
        ustr.encode() call.q�h�hK�K	�q h]r  }r  (h(}r  (hh�h	h#u}r  (hh�h	]r  (hhh h#h%eu}r  (hh'h	]r  (h+hehrhsu}r  (hh�h	h hrX   'strict'r	  utr
  h2NuauuX   _encode_chunksr  }r  (hhh}r  (hNh�hMLK	�r  h]r  }r  (h}r  (hh�h	h#u}r  (hX	   newchunksr  h	]r  h
he]r  Na�r  au}r  (hh-h	]r  (h0hh%eu�r  h2h uauuX   __ne__r  }r  (hhh}r  (hNh�hK�K	�r  h]r  }r  (h}r   (hh�h	h#u}r!  (hh�h	Nu�r"  h2NuauuX   __str__r#  }r$  (hhh}r%  (hX   A synonym for self.encode().r&  h�hK�K	�r'  h]r(  }r)  (h}r*  (hh�h	h#u�r+  h2h uauuX   __unicode__r,  }r-  (hhh}r.  (hX)   Helper for the built-in unicode function.r/  h�hK�K	�r0  h]r1  }r2  (h}r3  (hh�h	h#u�r4  h2huauuX   _charsetr5  }r6  (hhMh}r7  h}r8  (hhh}r9  h	hsu}r:  (hhh}r;  h	h+su�r<  suX   _continuation_wsr=  }r>  (hhh}r?  h	h suX   _chunksr@  }rA  (hhh}rB  h	h�suX   _firstlinelenrC  }rD  (hhMh}rE  h}rF  (hhh}rG  h	hsu}rH  (hhh}rI  h	h%su�rJ  suX   _maxlinelenrK  }rL  (hhh}rM  h	h0suuhNh�hK�K�rN  uuX   HeaderParseErrorrO  }rP  (hhFh]rQ  X   email.errorsrR  X   HeaderParseErrorrS  �rT  auX   UTF8rU  }rV  (hhh}rW  h	h+suX   NLrX  }rY  (hhh}rZ  h	h suX   ecrer[  }r\  (hhMh}r]  h}r^  (hhh}r_  h	h%su}r`  (hhh}ra  h	hUsu�rb  suX   _max_appendrc  }rd  (hX   funcrefre  h}rf  X	   func_namerg  X   email.quoprimime._max_appendrh  suX   decode_headerri  }rj  (hhh}rk  (hX�  Decode a message header value without converting charset.

    Returns a list of (decoded_string, charset) pairs containing each of the
    decoded parts of the header.  Charset is None for non-encoded parts of the
    header, otherwise a lower-case string containing the name of the character
    set specified in the encoded string.

    An email.errors.HeaderParseError may be raised when certain decoding error
    occurs (e.g. a base64 decoding exception).rl  h�hK9K�rm  h]rn  }ro  (h}rp  (hX   headerrq  h	h u�rr  h2]rs  (h
he]rt  h
h4]ru  (h he�rv  a�rw  h�euauuuhX+   Header encoding and decoding functionality.rx  X   childrenry  ]rz  X   filenamer{  X   c:\python27\lib\email\header.pyr|  u.