�}q (X   membersq}q(X   sysq}q(X   kindqX	   modulerefqX   valueqX   sysqX    q	�q
uX   paramq}q(hX   typeqh}q(X   mroq]qX&   numpy.testing.nose_tools.parameterizedqh�qaX   basesq]qh}q(X   from_decoratorq}q(hX   functionqh}q(X   docqX   Returns an instance of ``param()`` for ``@parameterized`` argument
            ``args``::

                >>> param.from_decorator((42, ))
                param(args=(42, ), kwargs={})
                >>> param.from_decorator("foo")
                param(args=("foo", ), kwargs={})qX   builtinq�X   locationqKsK	�qX	   overloadsq]q }q!(X   argsq"}q#(X   nameq$X   clsq%hhu}q&(h$X   argsq'h]q((X   __builtin__q)X   strq*�q+h)X   tupleq,]q-]q.(X   numpy.core.multiarrayq/X   ndarrayq0�q1h)X   boolq2�q3h)X   objectq4�q5ea�q6h)X
   basestringq7�q8heu�q9X   ret_typeq:huaX   classmethodq;�uuX   explicitq<}q=(hhh}q>(hX   Creates a ``param`` by explicitly specifying ``args`` and
            ``kwargs``::

                >>> param.explicit([1,2,3])
                param(*(1, 2, 3))
                >>> param.explicit(kwargs={"foo": 42})
                param(*(), **{"foo": "42"})q?h�hKeK	�q@h]qA}qB(h"}qC(h$h%hhu}qD(h$h'h]qE(h)h,�qFh)X   NoneTypeqG�qHeX   default_valueqIX   NoneqJu}qK(h$X   kwargsqLh]qM(h)X   dictqN�qOhHehIhJu�qPh:Nuah;�uuX   __repr__qQ}qR(hhh}qS(hNh�hK�K	�qTh]qU}qV(h"}qW(h$X   selfqXhhu�qYh:h+uauuX   __new__qZ}q[(hhh}q\(hNh�hKaK	�q]h]q^}q_(h"}q`(h$h%hhu}qa(X
   arg_formatqbX   *qch$h'h]qd(h8h6hHhh+eu}qe(hbX   **qfh$hLh]qg(hHhOeu�qhh:Nuah;�uuuhX�   Represents a single parameter to a test case.

        For example::

            >>> p = param("foo", bar=16)
            >>> p
            param("foo", bar=16)
            >>> p.args
            ('foo', )
            >>> p.kwargs
            {'bar': 16}

        Intended to be used as an argument to ``@parameterized``::

            @parameterized([
                param("foo", bar=16),
            ])
            def test_stuff(foo, bar=16):
                passqih�hKKK�qjuuX   default_name_funcqk}ql(hhh}qm(hNh�hK�K�qnh]qo}qp(h"}qq(h$X   funcqrhNu}qs(h$X   numqthNu}qu(h$X   pqvhNu�qwh:NuauuX   PY2qx}qy(hX   dataqzh}q{hNsuX   default_doc_funcq|}q}(hhh}q~(hNh�hK�K�qh]q�}q�(h"}q�(h$hrhNu}q�(h$hthNu}q�(h$hvhhu�q�h:]q�(hHh+euauuX
   MethodTypeq�}q�(hhzh}q�hh)h�q�suX   req�}q�(hhhX   req�h	�q�uX   QuietOrderedDictq�}q�(hhh}q�(h]q�(hh��q�X   collectionsq�X   OrderedDictq��q�X   _abcollq�X   MutableMappingq��q�h�X   Mappingq��q�h�X   Sizedq��q�h�X   Iterableq��q�h�X	   Containerq��q�hOh5eh]q�(h�hOeh}q�(hQ}q�(hX   methodq�hh�uX   __str__q�}q�(hh�hh�uuhXt    When OrderedDict is available, use it to make sure that the kwargs in
        doc strings are consistently ordered.q�h�hK�K�q�uuX   TestCaseq�}q�(hX   typerefq�h]q�X   unittest.caseq�X   TestCaseq��q�auX
   short_reprq�}q�(hhh}q�(hX�    A shortened repr of ``x`` which is guaranteed to be ``unicode``::

            >>> short_repr("foo")
            u"foo"
            >>> short_repr("123456789", n=4)
            u"12...89"q�h�hK�K�q�h]q�}q�(h"}q�(h$X   xq�hNu}q�(h$X   nq�hh)X   intq��q�hIX   64q�u�q�h:]q�(h)X   unicodeq��q�h+euauuX   detect_runnerq�}q�(hhh}q�(hX	   Guess which test runner we're using by traversing the stack and looking
        for the first matching module. This *should* be reasonably safe, as
        it's done during test disocvery where the test runner should be the
        stack frame immediately outside.q�h�hMK�q�h]q�}q�(h")h:]q�(h+h3hHeuauuX   _test_runnersq�}q�(hhzh}q�hh)X   setqΆq�suX   string_typesq�}q�(hX   multipleq�h}q�h}q�(hhzh}q�hh)h,]q�h+a�q�su}q�(hhzh}q�hh)h,]q�h8a�q�su�q�suX   inspectq�}q�(hhhX   inspectq�h	�q�uX
   namedtupleq�}q�(hX   funcrefq�h}q�X	   func_nameq�X   collections.namedtupleq�suX   _test_runner_overrideq�}q�(hhzh}q�hhHsuX   InstanceTypeq�}q�(hh�h}q�h}q�(hhh}q�(h]q�hh�q�ah]q�h}q�hNh�hK6K�q�uu}q�(hhzh}q�hh�su�q�suX   PY3q�}q�(hhzh}q�hNsuX   set_test_runnerq�}q�(hhh}q�(hNh�hMK�q�h]q�}q�(h"}q (h$X   namer  hNu�r  h:NuauuX"   parameterized_argument_value_pairsr  }r  (hhh}r  (hX  Return tuples of parameterized arguments and their values.

        This is useful if you are writing your own doc_func
        function and need to know the values for each parameter name::

            >>> def func(a, foo=None, bar=42, **kwargs): pass
            >>> p = param(1, foo=7, extra=99)
            >>> parameterized_argument_value_pairs(func, p)
            [("a", 1), ("foo", 7), ("bar", 42), ("**kwargs", {"extra": 99})]

        If the function's first argument is named ``self`` then it will be
        ignored::

            >>> def func(self, a): pass
            >>> p = param(1)
            >>> parameterized_argument_value_pairs(func, p)
            [("a", 1)]

        Additionally, empty ``*args`` or ``**kwargs`` will be ignored::

            >>> def func(foo, *args): pass
            >>> p = param(1)
            >>> parameterized_argument_value_pairs(func, p)
            [("foo", 1)]
            >>> p = param(1, 16)
            >>> parameterized_argument_value_pairs(func, p)
            [("foo", 1), ("*args", (16, ))]r  h�hK�K�r  h]r  }r	  (h"}r
  (h$hrhNu}r  (h$hvhhu�r  h:]r  (h)X   listr  ]r  ]r  (h5h)j  �r  h)h�r  h+hFhHh�X   distutils.distr  X   Distributionr  �r  h�X   distutils.fancy_getoptr  X   OptionDummyr  �r  X   tarfiler  X   TarFiler  �r  X   xml.sax.xmlreaderr  X   InputSourcer  �r  ea�r  j  euauuX	   text_typer   }r!  (hh�h}r"  h}r#  (hh�h]r$  h+au}r%  (hh�h]r&  h�au�r'  suX   warningsr(  }r)  (hhhX   warningsr*  h	�r+  uX   wrapsr,  }r-  (hh�h}r.  h�X   functools.wrapsr/  suX
   bytes_typer0  }r1  (hh�h]r2  h+auX   _paramr3  }r4  (hhzh}r5  hNsuX   MaybeOrderedDictr6  }r7  (hh�h}r8  h}r9  (hh�h]r:  h�au}r;  (hh�h]r<  hOau�r=  suX   parameterizedr>  }r?  (hhh}r@  (h]rA  (hj>  �rB  h5eh]rC  h5ah}rD  (X   __init__rE  }rF  (hhh}rG  (hNh�hM=K	�rH  h]rI  }rJ  (h"}rK  (h$hXhjB  u}rL  (h$X   inputrM  hNu}rN  (h$X   doc_funcrO  hhHhIhJu�rP  h:NuauuX   check_input_valuesrQ  }rR  (hhh}rS  (hNh�hM�K	�rT  h]rU  }rV  (h"}rW  (h$h%hjB  u}rX  (h$X   input_valuesrY  h]rZ  h)j  ]r[  Na�r\  au�r]  h:]r^  h)j  ]r_  ha�r`  auah;�uuX   expandra  }rb  (hhh}rc  (hX`   A "brute force" method of parameterizing test cases. Creates new
            test cases and injects them into the namespace that the wrapped
            function is being defined in. Useful for parameterizing tests in
            subclasses of 'UnitTest', where Nose test generators don't work.

            >>> @parameterized.expand([("foo", 1, 2)])
            ... def test_add1(name, input, expected):
            ...     actual = add1(input)
            ...     assert_equal(actual, expected)
            ...
            >>> locals()
            ... 'test_add1_foo_0': <function ...> ...
            >>>rd  h�hM�K	�re  h]rf  }rg  (h"(}rh  (h$h%hjB  u}ri  (h$jM  hNu}rj  (h$X	   name_funcrk  h]rl  (j  hHehIhJu}rm  (h$jO  h]rn  (j  hHehIhJu}ro  (hbhfh$X   legacyrp  hhOutrq  h:j  uah;�uuX   to_safe_namerr  }rs  (hhh}rt  (hNh�hM�K	�ru  h]rv  }rw  (h"}rx  (h$h%hjB  u}ry  (h$X   srz  hNu�r{  h:h+uah;�uuX   param_as_nose_tupler|  }r}  (hhh}r~  (hNh�hMhK	�r  h]r�  }r�  (h"(}r�  (h$hXhjB  u}r�  (h$X	   test_selfr�  hhHu}r�  (h$hrhNu}r�  (h$hthNu}r�  (h$hvhhutr�  h:]r�  h)h,]r�  (]r�  (j  h5eNe�r�  auauuX   input_as_callabler�  }r�  (hhh}r�  (hNh�hM�K	�r�  h]r�  }r�  (h"}r�  (h$h%hjB  u}r�  (h$jM  hNu�r�  h:]r�  j  auah;�uuX   assert_not_in_testcase_subclassr�  }r�  (hhh}r�  (hNh�hM{K	�r�  h]r�  }r�  (h"}r�  (h$hXhjB  u�r�  h:NuauuX   param_as_standalone_funcr�  }r�  (hhh}r�  (hNh�hM�K	�r�  h]r�  }r�  (h"(}r�  (h$h%hjB  u}r�  (h$hvhNu}r�  (h$hrhNu}r�  (h$j  hNutr�  h:]r�  j  auah;�uuX$   _terrible_magic_get_defining_classesr�  }r�  (hhh}r�  (hX2   Returns the set of parent classes of the class currently being defined.
            Will likely only work if called from the ``parameterized`` decorator.
            This function is entirely @brandon_rhodes's fault, as he suggested
            the implementation: http://stackoverflow.com/a/8793684/71522r�  h�hM�K	�r�  h]r�  }r�  (h"}r�  (h$hXhjB  u�r�  h:]r�  (j  j\  h5euauuX   __call__r�  }r�  (hhh}r�  (hNh�hMAK	�r�  h]r�  }r�  (h"}r�  (h$hXhjB  u}r�  (h$X	   test_funcr�  hNu�r�  h:j  uauuX	   get_inputr�  }r�  (hh�h}r�  h(}r�  (hhh}r�  (hNh�hKK�r�  h]r�  }r�  (h")h:j`  uauu}r�  (hhh}r�  (hNh�hKK�r�  h]r�  }r�  (h")h:j`  uauu}r�  (hhh}r�  (hNh�hKK�r�  h]r�  }r�  (h")h:j`  uauu}r�  (hhh}r�  (hNh�hKK�r�  h]r�  }r�  (h")h:j`  uauutr�  sujO  }r�  (hh�h}r�  h}r�  (hhh}r�  (hNh�hK�K�r�  h]r�  }r�  (h"}r�  (h$hrhNu}r�  (h$hthNu}r�  (h$hvhhu�r�  h:]r�  (hHh+euauu}r�  (hhzh}r�  hhHsu�r�  suuhX<   Parameterize a test case::

            class TestInt(object):
                @parameterized([
                    ("A", 10),
                    ("F", 15),
                    param("10", 42, base=42)
                ])
                def test_int(self, input, expected, base=16):
                    actual = int(input, base=base)
                    assert_equal(actual, expected)

            @parameterized([
                (2, 3, 5)
                (3, 5, 8),
            ])
            def test_add(a, b, expected):
                assert_equal(a + b, expected)r�  h�hM(K�r�  uuX   _test_runner_aliasesr�  }r�  (hhzh}r�  hhOsuX   make_methodr�  }r�  (hh�h}r�  h}r�  (hhh}r�  (hNh�hK<K	�r�  h]r�  }r�  (h"}r�  (h$hrh]r�  (h5j  eu}r�  (h$X   instancer�  hhHu}r�  (h$hhh�u�r�  h:]r�  (h5j  euauu}r�  (hhh}r�  (hNh�hKFK	�r�  h]r�  }r�  (h"}r�  (h$hrh]r   (h5j  eu}r  (h$j�  hhHu}r  (h$hhh�u�r  h:h5uauu�r  suX   lzipr  }r  (hh�h}r  h}r  (hhh}r	  (hNh�hKK�r
  h]r  }r  (h"}r  (hbhch$X   ar  h]r  (h)h,]r  Na�r  hFeu�r  h:]r  (j  j\  euauu}r  (hh�h}r  h�X   __builtin__.zipr  su�r  suX   _test_runner_guessr  }r  (hh�h}r  h(}r  (hhzh}r  hhHsu}r  (hhzh}r  hh+su}r  (hhzh}r   hh3su}r!  (hhzh}r"  hh+sutr#  suuhXq  
tl;dr: all code code is licensed under simplified BSD, unless stated otherwise.

Unless stated otherwise in the source files, all code is copyright 2010 David
Wolever <david@wolever.net>. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL <COPYRIGHT HOLDER> OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of David Wolever.r$  X   childrenr%  ]r&  X   filenamer'  X�   c:\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\lib\site-packages\numpy\testing\nose_tools\parameterized.pyr(  u.