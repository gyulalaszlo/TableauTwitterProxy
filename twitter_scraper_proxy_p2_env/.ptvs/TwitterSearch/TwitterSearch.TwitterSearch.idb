�}q (X   membersq}q(X   TwitterOrderq}q(X   kindqX   typerefqX   valueq]qX   TwitterSearch.TwitterOrderq	X   TwitterOrderq
�qauX   parse_qsq}q(hX   funcrefqh}qX	   func_nameqX   urlparse.parse_qsqsuX   requestsq}q(hX	   modulerefqhX   requestsqX    q�quX   TwitterSearchq}q(hX   typeqh}q(X   mroq]q(X   TwitterSearch.TwitterSearchqh�qX   __builtin__q X   objectq!�q"eX   basesq#]q$h"ah}q%(X   check_http_statusq&}q'(hX   functionq(h}q)(X   docq*X�   Checks if given HTTP status code is within the list at          ``TwitterSearch.exceptions`` and raises a ``TwitterSearchException``          if this is the case. Example usage: ``checkHTTPStatus(200)`` and          ``checkHTTPStatus(401)``

        :param http_status: Integer value of the HTTP status of the         last query. Invalid statuses will raise an exception.
        :raises: TwitterSearchExceptionq+X   builtinq,�X   locationq-K�K	�q.X	   overloadsq/]q0}q1(X   argsq2}q3(X   nameq4X   selfq5hhu}q6(h4X   http_statusq7hNu�q8X   ret_typeq9NuauuX	   _lang_urlq:}q;(hX   dataq<h}q=hh X   strq>�q?suX
   exceptionsq@}qA(hh<h}qBhh X   dictqC�qDsuX   __iter__qE}qF(hh(h}qG(h*Nh,�h-M�K	�qHh/]qI}qJ(h2}qK(h4h5hhu�qLh9huauuX   __init__qM}qN(hh(h}qO(h*Xw   Constructor

        :param consumer_key: Consumer key (app related)
        :param consumer_secret: Consumer consumer_secret (app related)
        :param access_token: Access token (user related)
        :param access_token_secret: Access token secret (user related)

        :param verify: A boolean variable to control verification of         access codes. Default value is ``True`` which         raises an instant exception when using invalid credentials.

        :param proxy: A string containing a HTTPS proxy         (e.g. ``my.proxy.com:8080``). Default value is ``None``         which means that no proxy is used at all.qPh,�h-KHK	�qQh/]qR}qS(h2(}qT(h4h5hhu}qU(h4X   consumer_keyqVhNu}qW(h4X   consumer_secretqXhNu}qY(h4X   access_tokenqZhNu}q[(h4X   access_token_secretq\hNu}q](X
   arg_formatq^X   **q_h4X   attrq`hhDutqah9NuauuX   get_metadataqb}qc(hh(h}qd(h*X   Returns all available meta data collected during last query.         See `Advanced usage <advanced_usage.html>`_ for example

        :returns: Available meta information about the         last query in form of a ``dict``
        :raises: TwitterSearchExceptionqeh,�h-MGK	�qfh/]qg}qh(h2}qi(h4h5hhu�qjh9]qk(hDh X   NoneTypeql�qmX   requests.structuresqnX   CaseInsensitiveDictqo�qpeuauuX   get_statisticsqq}qr(hh(h}qs(h*X   Returns dict with statistical information about         amount of queries and received tweets. Returns statistical values         about the number of queries and the sum of all tweets received by         this very instance of :class:`TwitterSearch`.         Example usage: ``print("Queries done: %i. Tweets received: %i"
        % ts.get_statistics())``

        :returns: A ``tuple`` with ``queries`` and         ``tweets`` keys containing integers. E.g. ``(1,100)`` which stands         for one query that contained one hundred tweets.qth,�h-M`K	�quh/]qv}qw(h2}qx(h4h5hhu�qyh9h X   tupleqz]q{(h X   intq|�q}h}e�q~uauuX   __next__q}q�(hh(h}q�(h*Nh,�h-M�K	�q�h/]q�}q�(h2}q�(h4h5hhu�q�h9h"uauuX   set_supported_languagesq�}q�(hh(h}q�(h*X    Loads currently supported languages from Twitter API         and sets them in a given TwitterSearchOrder instance.
        See `Advanced usage <advanced_usage.html>`_ for example

        :param order: A TwitterOrder instance.         Can be either TwitterSearchOrder or TwitterUserOrderq�h,�h-M}K	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4X   orderq�hNu�q�h9NuauuX   _search_urlq�}q�(hh<h}q�hh?suX   nextq�}q�(hh(h}q�(h*X|    Python2 comparability method. Simply returns ``self.__next__()``

        :returns: the ``__next__()`` method of this classq�h,�h-M�K	�q�h/]q�}q�(h2}q�(h4h5hhu�q�h9h"uauuX	   _base_urlq�}q�(hh<h}q�hh?suX   send_searchq�}q�(hh(h}q�(h*Xi   Queries the Twitter API with a given query string and         stores the results internally. Also validates returned HTTP status         code and throws an exception in case of invalid HTTP states.         Example usage ``sendSearch('?q=One+Two&count=100')``

        :param url: A string of the URL to send the query to
        :raises: TwitterSearchExceptionq�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4X   urlq�hh?u�q�h9]q�h hz]q�(]q�(hphmhDe]q�(hphmhDee�q�auauuX	   get_proxyq�}q�(hh(h}q�(h*X�    Returns the current proxy url or None if no proxy is set

        :returns: A string containing the current HTTPS proxy         (e.g. ``my.proxy.com:8080``) or ``None`` is no proxy is usedq�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu�q�h9]q�(h?h X
   basestringq��q�hmeuauuX   authenticateq�}q�(hh(h}q�(h*XX   Creates an authenticated and internal oauth2  handler needed for         queries to Twitter and verifies credentials if needed.  If ``verify``         is true, it also checks if the user credentials are valid.         The **default** value is *True*

        :param verify: boolean variable to         directly check. Default value is ``True``q�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4X   verifyq�hh X   boolqǆq�X   default_valueq�X   Trueq�u�q�h9NuauuX   get_minimal_idq�}q�(hh(h}q�(h*X�    Returns the minimal tweet ID of the current response

        :returns: minimal tweet identification number
        :raises: TwitterSearchExceptionq�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu�q�h9h}uauuX   search_tweets_iterableq�}q�(hh(h}q�(h*X�   Returns itself and queries the Twitter API. Is called when using         an instance of this class as iterable.         See `Basic usage <basic_usage.html>`_ for examples

        :param order: An instance of TwitterOrder class         (e.g. TwitterSearchOrder or TwitterUserOrder)
        :param callback: Function to be called after a new page         is queried from the Twitter API
        :returns: Itself using ``self`` keywordq�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4h�hNu}q�(h4X   callbackq�hhmh�X   Noneq�u�q�h9huauuX	   _user_urlq�}q�(hh<h}q�hh?suX   search_tweetsq�}q�(hh(h}q�(h*X   Creates an query string through a given TwitterSearchOrder         instance and takes care that it is send to the Twitter API.         This method queries the Twitter API **without** iterating or         reloading of further results and returns response.         See `Advanced usage <advanced_usage.html>`_ for example

        :param order: A TwitterOrder instance.         Can be either TwitterSearchOrder or TwitterUserOrder
        :returns: Unmodified response as ``dict``.
        :raises: TwitterSearchExceptionq�h,�h-MK	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4h�h]q�(X    TwitterSearch.TwitterSearchOrderq�X   TwitterSearchOrderq��q�X   TwitterSearch.TwitterUserOrderq�X   TwitterUserOrderq�q�eu�q�h9hDuauuX   get_amount_of_tweetsq�}q�(hh(h}q�(h*X�    Returns current amount of tweets available within this instance

        :returns: The amount of tweets currently available
        :raises: TwitterSearchExceptionq�h,�h-MoK	�q�h/]q�}q�(h2}q�(h4h5hhu�q�h9h}uauuX	   set_proxyq�}q (hh(h}r  (h*X�    Sets a HTTPS proxy to query the Twitter API

        :param proxy: A string of containing a HTTPS proxy         e.g. ``set_proxy("my.proxy.com:8080")``.
        :raises: TwitterSearchExceptionr  h,�h-K�K	�r  h/]r  }r  (h2}r  (h4h5hhu}r  (h4X   proxyr  h]r	  (h�h?eu�r
  h9NuauuX   __repr__r  }r  (hh(h}r  (h*X�    Returns the class and its access token

        :returns: A string represenation of this         class containing the class name and the used access tokenr  h,�h-KxK	�r  h/]r  }r  (h2}r  (h4h5hhu�r  h9h?uauuX   _verify_urlr  }r  (hh<h}r  hh?suX   search_next_resultsr  }r  (hh(h}r  (h*XG   Triggers the search for more results using the Twitter API.         Raises exception if no further results can be found.         See `Advanced usage <advanced_usage.html>`_ for example

        :returns: ``True`` if there are more results available         within the Twitter Search API
        :raises: TwitterSearchExceptionr  h,�h-M5K	�r  h/]r  }r  (h2}r  (h4h5hhu�r  h9h�uauuX
   get_tweetsr   }r!  (hh(h}r"  (h*X�    Returns all available data from last query.         See `Advanced usage <advanced_usage.html>`_ for example

        :returns: All tweets found using the last query as a ``dict``
        :raises: TwitterSearchExceptionr#  h,�h-MTK	�r$  h/]r%  }r&  (h2}r'  (h4h5hhu�r(  h9]r)  (hDhmhpeuauuX   _TwitterSearch__consumer_keyr*  }r+  (hh<h}r,  hNsuX   _TwitterSearch__consumer_secretr-  }r.  (hh<h}r/  hNsuX   _TwitterSearch__access_tokenr0  }r1  (hh<h}r2  hNsuX#   _TwitterSearch__access_token_secretr3  }r4  (hh<h}r5  hNsuX   _TwitterSearch__responser6  }r7  (hh<h}r8  hhDsuX   _TwitterSearch__nextMaxIDr9  }r:  (hh<h}r;  hh}suX   _TwitterSearch__next_tweetr<  }r=  (hX   multipler>  h}r?  h}r@  (hh<h}rA  hh}su}rB  (hh<h}rC  hh}su�rD  suX   _TwitterSearch__proxyrE  }rF  (hj>  h}rG  h}rH  (hh<h}rI  hhmsu}rJ  (hh<h}rK  hh�su}rL  (hh<h}rM  hh?su�rN  suX   _TwitterSearch__statisticsrO  }rP  (hh<h}rQ  hh X   listrR  ]rS  (h}h}e�rT  suX   _TwitterSearch__callbackrU  }rV  (hh<h}rW  hhmsuX   _TwitterSearch__oauthrX  }rY  (hh<h}rZ  hX   requests_oauthlib.oauth1_authr[  X   OAuth1r\  �r]  suX   _TwitterSearch__order_is_searchr^  }r_  (hh<h}r`  hh�suX   _TwitterSearch__next_max_idra  }rb  (hj>  h}rc  h}rd  (hh<h}re  hhmsu}rf  (hh<h}rg  hh}su�rh  suX
   _start_urlri  }rj  (hj>  h}rk  h}rl  (hh<h}rm  hh?su}rn  (hh<h}ro  hh?su}rp  (hh<h}rq  hh?su�rr  suuh*X-  
    This class contains the actual functionality of this library. 
    It is responsible for correctly transmitting your data to the Twitter API 
    (v1.1 only) and returning the results to your program afterwards.
    It is configured using an implementation of :class:`TwitterOrder` 
    along with valid Twitter credentials. Currently two different
    implementations are usable: :class:`TwitterUserOrder` for retrieving the
    timeline of a certain user and :class:`TwitterSearchOrder` for accessing
    the Twitter Search API.

    The methods ``next()``, ``__next__()`` and ``__iter__()`` are used 
    during the iteration process. For more information about those 
    methods please consult the `official Python
    documentation
    <http://docs.python.org/2/library/stdtypes.html#iterator-types>`_.rs  h,�h-KK�rt  uuX   TwitterUserOrderru  }rv  (hhh]rw  h�auX   TwitterSearchExceptionrx  }ry  (hhh]rz  X$   TwitterSearch.TwitterSearchExceptionr{  X   TwitterSearchExceptionr|  �r}  auX   py3kr~  }r  (hh<h}r�  hNsuX   OAuth1r�  }r�  (hhh]r�  j]  auX   maxintr�  }r�  (hh<h}r�  hh}suX   TwitterSearchOrderr�  }r�  (hhh]r�  h�auuh*hX   childrenr�  ]r�  X   filenamer�  X�   c:\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\lib\site-packages\TwitterSearch\TwitterSearch.pyr�  u.