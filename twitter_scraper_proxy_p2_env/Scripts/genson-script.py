#!C:\Twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_p2\twitter_scraper_proxy_p2_env\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'genson==1.0.0','console_scripts','genson'
__requires__ = 'genson==1.0.0'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('genson==1.0.0', 'console_scripts', 'genson')()
    )
